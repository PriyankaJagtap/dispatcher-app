//
//  WebserviceHelper.swift
//  ObjectMapper_Demo
//
//  Created by Webwerks on 09/03/17.
//  Copyright © 2017 Webwerks. All rights reserved.
//

import UIKit
import Alamofire

let secret_Token = "KpvhKq2p0UIyGjdbFH90XbWHefj0qR"
let secret_Key = "tfYoCKgpuM4ikBe"

enum MethodType
{
case get
case post
}

enum APIEndUrl
{
    case userLogin
    case caseList
    case caseDetails
    case getDrivers
    case saveCompleteCaseDetails
    case caseClosing
    case caseComment
    case addJobToJobList
    case updateCaseStatus
    case forgotPassword
    case logout
    case caseTruckList
    case problemServices
    case vehiclebrandsAndModels
    case caseTruckRegister
    case caseServiceAndBrand
    case caseUpdateTruck
    
    case caseDriverRegistration
    case caseAllJobList
    case caseJobStatus
    case caseJobShift
    
    case caseDriverDetail
}

///API constant
struct WSAPIConstant {
//    static var API_BASEURL = "http://staging.php-dev.in:8844/KidFit/KidFit/wp-json/apps/v1/"
//    static var API_BASEURL = "http://dispatch.tracuae.com/"
    static var API_BASEURL = "http://rsa.carpal.com/"
    static let WAITING_FOR_NETWORK = "Waiting for Network"
    static let ERROR_MESSAGE = "Error occured. PLease try again later"
}

// MARK: Webservices
class WebserviceHelper: NSObject {

    
    func callWebservice( method:MethodType, parameters:Dictionary<String, AnyObject>, endUrl:APIEndUrl, completionHandler: @escaping ( _ r: String) -> Void)
    {
        let path = WSAPIConstant.API_BASEURL + self.endUrlPath(endUrl)
        print("path :\(path)\nParameters :\(parameters)")
        let token : String
        //        if((UserDefault.accessToken) != nil){
        token = "bearer"+" "+"0a94b45c-dc40-4250-91da-6957834c0433"
        //    }
        //        else{
        //            token = ""
        //        }
        switch method {
        case .post:
            
            Alamofire.request(path, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers:["Authorization": token]) .responseJSON
                { response in
                    print("\(String(describing: response.result.error))")
                    
                    if let JSON = String(data: response.data!, encoding: String.Encoding.utf8) {
                        print("JSON: \(JSON)")
                        completionHandler(JSON)
                        
                    }
                    
            }
        case .get:
            Alamofire.request(path,method: .get, parameters: parameters, encoding: URLEncoding.default, headers:["Authorization": token]) .responseJSON
                { response in
                   // let dict :NSDictionary = response.result.value as! NSDictionary
                   // print("dict\(dict)")
                    
                    if let JSON = String(data: response.data!, encoding: String.Encoding.utf8)  {
                        print("JSON: \(JSON)")
                        completionHandler(JSON)
                    }
            }
        }
    }
    

    ///Endurl
    func endUrlPath(_ endUrl : APIEndUrl) -> String {
        switch endUrl {
        case .userLogin:
            return "user/validate"
        case .caseList:
            return "cases/listAllCases"
        case .caseDetails:
            return "case/details"
        case .getDrivers:
            return "driver/fetchDriversInfo"
        case.saveCompleteCaseDetails:
            return "caseRegistration/saveCaseInfo"
        case .caseClosing:
            return "customerStaisfaction/saveCustomerReview"
        case .caseComment:
            return "/case/saveComment"
        case .addJobToJobList:
            return "/caseOnJob/addOnJobDetails"
        case .updateCaseStatus:
            return "/caseSchedule/updateCaseArrivalTime"
        case.forgotPassword:
            return "/tablet/forgotPassword"
        case.logout:
            return "/logout"
        case .caseTruckList:
            return "/truck/gettruckinfo"
        case .problemServices:
            return "/countryCurrencyTimeProblemServicesClient/fetch?"
        case .vehiclebrandsAndModels:
            return "/vehicle/getvehiclebrands"
        case.caseTruckRegister:
            return "truck/register"
        case.caseServiceAndBrand:
            return "/truck/getservicesandbrands"
        case.caseUpdateTruck:
            return "truck/update"
            
        case.caseDriverRegistration:
            return "driver/saveAllDetailsOfDriver"
        case.caseAllJobList:
            return "/cases/listAllJobByDispatcher"
        case.caseJobStatus:
            return "caseRegistration/driverStatuses"
        case.caseJobShift:
            return "driver/gettrucksandshiftsmanagers"
            
        case.caseDriverDetail:
            return "driver/getFullDriverDetails"
            
        }
    }
    // MARK: Login WS
    func callPlaylistApi(completionHandler: @escaping (_ r: String) -> Void)
    {
        
        let request: [String: AnyObject] = ["type":"user" as AnyObject,
                                            "user_id":"3" as AnyObject,
                                            "secret_token":secret_Token as AnyObject,
                                            "secret_key":secret_Key as AnyObject]
        
        self.callWebservice(method: MethodType.get, parameters: request, endUrl: APIEndUrl.userLogin) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Login WS
    func callLoginApi(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.userLogin) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get Case List WS
    func callGetCaseListApi(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseList) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get Case List WS
    func callGetCaseDetailsAPi(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseDetails) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get Case List WS
    func callGetDriversListAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.get, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.getDrivers) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get Case List WS
    func callSaveCompleteCaseDetailsAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.saveCompleteCaseDetails) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get Case Closing WS
    func callCaseClosingAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseClosing) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get Comment WS
    func callCommentAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseComment) { (resp) in
            
            completionHandler(resp)
            
        }
    }
    
    // MARK: Add Job WS
    func callAddJobToJobListAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.addJobToJobList) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Update Status WS
    func callUpdateCaseStatusAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.updateCaseStatus) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    //MARK: Forgot Password WS
    func callForgotPasswordApi(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.forgotPassword) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    //MARK: Logout WS
    func callLogoutApi(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.logout) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get TruckList WS
    func calltruckListAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseTruckList) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get TruckList WS
    func callGetProblemServicesAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.get, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.problemServices) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get Vehicle Brands and Models WS
    func callGetVehicleBrandAndModelsAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.get, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.vehiclebrandsAndModels) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get TruckRegister WS
    func calltruckRegisterAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseTruckRegister) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get TruckRegister WS
    func callGetServiceAndBrandAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.get, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseServiceAndBrand) { (resp) in
            
            completionHandler(resp)
        }
    }
    // MARK: Get Update Truck WS
    func callUpdateTruckAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseUpdateTruck) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get Driver Registration WS
    func callRegisterDriverAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseDriverRegistration) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    
    // MARK: Get All Jobs WS
    func callAllJobsDispatcherAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseAllJobList) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get All Jobs Status WS
    func callJobsStatusDispatcherAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.get, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseJobStatus) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get Jobs Shift WS
    func callTruckAndShiftManagerAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.get, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseJobShift) { (resp) in
            
            completionHandler(resp)
        }
    }
    
    // MARK: Get Update Truck WS
    func callDriverDetailAPI(parameters: NSMutableDictionary ,completionHandler: @escaping (_ r: String) -> Void)
    {
        
        self.callWebservice(method: MethodType.post, parameters: parameters as! Dictionary<String, AnyObject>, endUrl: APIEndUrl.caseDriverDetail) { (resp) in
            
            completionHandler(resp)
        }
    }

    
}
