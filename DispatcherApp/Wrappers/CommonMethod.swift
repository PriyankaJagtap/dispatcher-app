//
//  CommonMethod.swift
//  ObjectMapper_Demo
//
//  Created by Webwerks on 15/03/17.
//  Copyright © 2017 Webwerks. All rights reserved.
//

import UIKit

class CommonMethod: NSObject {

    //MARK: AlertMessage
    func alertMessage(_ title:NSString, message:NSString, currentVC:UIViewController ) -> Void {
        
        let alert = UIAlertController(title: title as String, message:message as String, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        DispatchQueue.main.async(execute: {
            currentVC.present(alert, animated: true, completion: nil)
            
        })
    }

    
    func setDate(date: Date) -> String! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
       let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
}
