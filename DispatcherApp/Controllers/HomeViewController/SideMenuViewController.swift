//
//  SideMenuViewController.swift
//  DispatcherApp
//
//  Created by Webwerks on 12/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var userProfileImgView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    var nameArray : NSMutableArray = []
    @IBOutlet weak var sideMenuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        userProfileImgView.layer.masksToBounds = true
        userProfileImgView.layer.cornerRadius = (userProfileImgView.frame.size.width)/2
        
        nameArray = ["Home","User Profile","Drivers List","Trucks List","Truck Registration","Driver Registration","Jobs","Logout"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell:SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell!

        let str:String = nameArray [indexPath.row] as! String

        cell.nameLabel.text = str
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        switch (indexPath as NSIndexPath).row {
        case 0:
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            let homeVC : UIViewController = UINavigationController(rootViewController: homeViewController)
            self.slideMenuController()?.changeMainViewController(homeVC, close: true)
            break
        case 1:
            let userProfileViewController = storyboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            let userProfileVc : UIViewController = UINavigationController(rootViewController: userProfileViewController)
            self.slideMenuController()?.changeMainViewController(userProfileVc, close: true)
        case 2:
            let driverListViewController = storyboard.instantiateViewController(withIdentifier: "DriverListViewController") as! DriverListViewController
            let driverListVc : UIViewController = UINavigationController(rootViewController: driverListViewController)
            self.slideMenuController()?.changeMainViewController(driverListVc, close: true)
            break
        case 3:
            let truckListViewController = storyboard.instantiateViewController(withIdentifier: "TruckListViewController") as! TruckListViewController
            let truckVc : UIViewController = UINavigationController(rootViewController: truckListViewController)
            self.slideMenuController()?.changeMainViewController(truckVc, close: true)
           break
        
            case 4:
            let truckRegViewController = storyboard.instantiateViewController(withIdentifier: "TruckRegistrationViewController") as! TruckRegistrationViewController
            let truckRegVc : UIViewController = UINavigationController(rootViewController: truckRegViewController)
            self.slideMenuController()?.changeMainViewController(truckRegVc, close: true)
            break
            
        case 5:
            let driverRegViewController = storyboard.instantiateViewController(withIdentifier: "DriversRegistrationViewController") as! DriversRegistrationViewController
            let driverRegVc : UIViewController = UINavigationController(rootViewController: driverRegViewController)
            self.slideMenuController()?.changeMainViewController(driverRegVc, close: true)
            break
        
        case 6:
            let allJobListDispatcher = storyboard.instantiateViewController(withIdentifier: "AllJobsDispatcherVC") as! AllJobsDispatcherVC
            let driverRegVc : UIViewController = UINavigationController(rootViewController: allJobListDispatcher)
            self.slideMenuController()?.changeMainViewController(driverRegVc, close: true)
            break
        default:
            break

    }
    }
   
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }


}
