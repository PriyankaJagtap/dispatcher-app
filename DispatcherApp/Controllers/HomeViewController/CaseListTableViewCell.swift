//
//  CaseListTableViewCell.swift
//  DispatcherApp
//
//  Created by varsha on 5/23/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import SwipeCellKit

class CaseListTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var lblCaseNo: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblVehicle: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
