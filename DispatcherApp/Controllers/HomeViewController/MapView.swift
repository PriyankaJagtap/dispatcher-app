//
//  MapView.swift
//  DispatcherApp
//
//  Created by varsha on 5/24/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import GoogleMaps

class MapView: UIView ,CLLocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.mapView.frame = self.frame
        self.mapView.center = self.center
        
    }
    class func instanceFromNib() -> MapView {
      
        return UINib(nibName: "MapView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MapView
    }

}
