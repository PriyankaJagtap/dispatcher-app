//
//  CustomerBasicDetailViewController.swift
//  Carpal_Varsha
//
//  Created by Webwerks on 29/05/17.
//  Copyright © 2017 Webwerks. All rights reserved.
//

import UIKit
import MBProgressHUD
import CountryCodeSelect

class CustomerBasicDetailViewController: UIViewController,UITextFieldDelegate {

    var caseStringID = ""
    @IBOutlet weak var countryCodeTextField: UITextField!
    
    @IBOutlet weak var phoneCodeTextField: UITextField!
    @IBOutlet weak var phoneNoTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var altPhoneNoTxtField: UITextField!
    
    @IBAction func editBtnClicked(_ sender: Any) {
    }
    
    lazy var countryCodePickerView: CountryCodePicker! = {
        let pickerView = CountryCodePicker()
        return pickerView
    }()
    
    lazy var countryCodePickerViewToolbar: UIToolbar! = {
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.sizeToFit()
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                         UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonPressed(_:)))]
        return toolbar
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCaseDetails()

        countryCodeTextField.inputView = countryCodePickerView
        countryCodeTextField.inputAccessoryView = countryCodePickerViewToolbar
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        //self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
       // self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }

    
    
    func getCaseDetails()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        if (SharedInstanceClass.sharedInstance.selectedCaseID != nil){
        
            let loginParams: [String: AnyObject] = ["userId" : "572" as AnyObject, "clientId" : "1" as AnyObject, "searchBy" : SharedInstanceClass.sharedInstance.selectedCaseID as AnyObject, "isCustomer" : true as AnyObject ,"isById" : true as AnyObject, "timeZone" : "Asia/Dubai" as AnyObject]
        
        WebserviceHelper() .callGetCaseDetailsAPi(parameters: loginParams as! NSMutableDictionary) { (response) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            let responceMsg = CaseDetails(JSONString : response)
            if responceMsg?.isSuccess == true{
                SharedInstanceClass.sharedInstance.casedetail = responceMsg!
                print("success")
                let caseDict = responceMsg?.caseDetailsDictionary
                let street = caseDict?.street
                
                let dict = SharedInstanceClass.sharedInstance.caseData?.caseDetailsDictionary
                self.phoneNoTextField.text = dict?.mobileNumber
                self.firstNameTextField.text = dict?.firstName
                self.lastNameTextField.text = dict?.lastName
                self.emailTextField.text = dict?.emailId
                self.altPhoneNoTxtField.text = dict?.alternateMobileNumber

            }
            else
            {
                //                CommonMethod().alertMessage("Status", message:"Please try again" as NSString , currentVC: self)
                print("error")
                
            }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: Textfiled Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func doneButtonPressed(_ sender: Any) {
        countryCodeTextField.text = countryCodePickerView.selectedCountryCode
        countryCodeTextField.resignFirstResponder()
    }

}
