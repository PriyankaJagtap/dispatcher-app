//
//  TruckListCustomCell.swift
//  DispatcherApp
//
//  Created by Webwerks on 15/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class TruckListCustomCell: UITableViewCell {

    @IBOutlet weak var truckCodeLabel: UILabel!
    
    @IBOutlet weak var issuesDateLbl: UILabel!
    
    @IBOutlet weak var expDateLbl: UILabel!
    
    @IBOutlet weak var regDueDateLbl: UILabel!
    
    @IBOutlet weak var caseAttendedDate: UILabel!
    
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var deactivateBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
