//
//  DiverListViewController.swift
//  DispatcherApp
//
//  Created by webwerks on 13/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD
import GoogleMaps

/// Point of Interest Item which implements the GMUClusterItem protocol.
class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
}

let kClusterItemCount = 7
var kCameraLatitude = -33.8
var kCameraLongitude = 151.2

class DriverListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource ,GMUClusterManagerDelegate,GMSMapViewDelegate{
    
    @IBOutlet weak var tableView : UITableView!
    
    
    var driverListDataArr : NSMutableArray = []
    var myview = MapView()
    private var clusterManager: GMUClusterManager!
    private var infoWindow = MapInfoWindow()
    
    // MARK: Needed to create the custom info window
    fileprivate var locationMarker : GMSMarker? = GMSMarker()

    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var mapBtn: UIButton!
    @IBAction func sideMenuBtnClick(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        tableView.separatorStyle = .none
        self.callDriverListAPI()
        // Do any additional setup after loading the view.
        
        
    }
    /// Randomly generates cluster items within some extent of the camera and
    /// adds them to the cluster manager.
    private func generateClusterItems() {
        let extent = 0.2
        for index in 1..<driverListDataArr.count {
             let driverData : DriverListData = driverListDataArr[index] as! DriverListData
            let lat = Float(driverData.driverGPSLocationLat!)! + Float(extent) * Float(randomScale())
            let lng = Float(driverData.driverGPSLocationLong!)! + Float(extent) * Float(randomScale())
            let name = driverData.driverName
            let item =
                POIItem(position: CLLocationCoordinate2DMake(CLLocationDegrees(lat), CLLocationDegrees(lng)), name: name!)
            clusterManager.add(item)
        }
    }
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func mapBtnClicked(_ sender: UIButton) {
        self.view .endEditing(true)
       
        if mapContainerView.isHidden{
            mapContainerView.isHidden = false
            if UIImage(named: "list") != nil {
                mapBtn.setImage(UIImage(named: "list"), for: .normal)
            }
            var isMapPresent = false
            let subviews =  mapContainerView.subviews
            for view in subviews{
                if view.isKind(of: MapView.self){
                    isMapPresent = true
                    break
                }
            }
            if !isMapPresent{
                myview  = MapView.instanceFromNib()
                mapContainerView.isHidden = false
                myview.frame = CGRect(x: 0, y: 0, width: mapContainerView.frame.size.width, height: mapContainerView.frame.size.height)
                
                myview.layoutSubviews()
                self.mapContainerView.addSubview(myview)
                myview.mapView?.isMyLocationEnabled = true
                
                if(driverListDataArr.count>=1){
                let driverData : DriverListData = driverListDataArr[0] as! DriverListData

                let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(driverData.driverGPSLocationLat!)!,longitude: CLLocationDegrees(driverData.driverGPSLocationLong!)!, zoom: 3)
                myview.mapView.animate(to: camera)
                }

                //Location Manager code to fetch current location
                //locationManager.delegate = self
              //  locationManager.startUpdatingLocation()
                
                //***************** Clustering ***************//
                // Set up the cluster manager with the supplied icon generator and
                // renderer.
                let iconGenerator = GMUDefaultClusterIconGenerator()
                let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
                let renderer = GMUDefaultClusterRenderer(mapView: myview.mapView,
                                                         clusterIconGenerator: iconGenerator)
                clusterManager = GMUClusterManager(map: myview.mapView, algorithm: algorithm,
                                                   renderer: renderer)
                
                // Generate and add random items to the cluster manager.
                generateClusterItems()
                
                // Call cluster() after items have been added to perform the clustering
                // and rendering on map.
                clusterManager.cluster()
                clusterManager.setDelegate(self, mapDelegate: self)

            }
        }else{
            if UIImage(named: "map_view") != nil {
                mapBtn.setImage(UIImage(named: "map_view"), for: .normal)
            }
            
            mapContainerView.isHidden = true
            infoWindow.removeFromSuperview()
            
        }

    }
    // MARK: Tableview Delegate and Datasource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return driverListDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId:String = "DriverListCell"
        let cell : DriverListTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellId) as? DriverListTableViewCell
        let driverData : DriverListData = driverListDataArr[indexPath.row] as! DriverListData
        cell?.lblDriverName.text = driverData.driverName
       
      
        cell?.lblDriverShiftTimings.text = driverData.driverShift
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let driverData : DriverListData = self.driverListDataArr[indexPath.row] as? DriverListData
        {
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let driverRegVC = storyboard.instantiateViewController(withIdentifier: "DriversRegistrationViewController") as! DriversRegistrationViewController
            driverRegVC.DriverId = driverData.driverId!
            driverRegVC.isFromDriverList = true
            navigationController?.pushViewController(driverRegVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    
    //MARK: Webservice call
    func callDriverListAPI() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let Id : String = "8"
        let region : String = "Asia/Dubai"
        let driverListParam : [String : AnyObject] = ["clientId": UserDefault.clientID as AnyObject, "timeZone": region as AnyObject]
        WebserviceHelper() .callGetDriversListAPI(parameters: driverListParam as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            print(response)
            let responseMsg = DriverList(JSONString: response)
            if(responseMsg?.isSuccess)!
            {
                print("success")
                self.driverListDataArr = responseMsg?.driverListArray as! NSMutableArray
                DispatchQueue.main.async {
                self.tableView.reloadData()
                }
                
            }
            else
            {
                CommonMethod().alertMessage("Error", message: "Server error", currentVC: self)
            }
        }
    }

    

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
       
        // Needed to create the custom info window
        locationMarker = marker
        infoWindow.removeFromSuperview()
        infoWindow = loadNiB()
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - 5
        if let poiItem = marker.userData as? POIItem {
        infoWindow.nameLbl.text = poiItem.name
        }
        self.view.addSubview(infoWindow)
        
        return false
    }
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 5
        }
    }
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    
    
    // MARK: Needed to create the custom info window
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
    // MARK: Needed to create the custom info window (this is optional)
    func loadNiB() -> MapInfoWindow{
        let infoWindow = MapInfoWindow.instanceFromNib() as! MapInfoWindow
        return infoWindow
    }
    

}
