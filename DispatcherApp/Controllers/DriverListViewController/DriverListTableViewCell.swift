//
//  DriverListTableViewCell.swift
//  DispatcherApp
//
//  Created by webwerks on 13/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class DriverListTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDriverSerialNumber : UILabel!
    @IBOutlet weak var lblDriverName : UILabel!
    @IBOutlet weak var lblDriverContactNumber : UILabel!
    @IBOutlet weak var lblDriverShiftTimings : UILabel!
    @IBOutlet weak var lblDriverExpiryDetail : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
