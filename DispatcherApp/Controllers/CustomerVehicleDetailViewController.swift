//
//  CustomerVehicleDetailViewController.swift
//  Carpal_Varsha
//
//  Created by Webwerks on 29/05/17.
//  Copyright © 2017 Webwerks. All rights reserved.
//

import UIKit

class CustomerVehicleDetailViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var vehicleRegisterNoTextField: UITextField!
    @IBOutlet weak var sourceCountryTextField: UITextField!
    @IBOutlet weak var regPlateNoTextField: UILabel!
    @IBOutlet weak var sourceCityTextField: UITextField!
    @IBOutlet weak var VINTextField: UITextField!
    @IBOutlet weak var modelTextField: UITextField!
    @IBOutlet weak var brandTextField: UITextField!
    @IBOutlet weak var mileageTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var modelYearTextField: UITextField!
    @IBOutlet weak var editBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

               
        let dict = SharedInstanceClass.sharedInstance.caseData?.caseDetailsDictionary

            vehicleRegisterNoTextField.text = dict?.vehicleRegisterNo
            sourceCountryTextField.text = dict?.customerLocationCountryName
            sourceCityTextField.text = dict?.customerLocationCity
            VINTextField.text = dict?.vin
            modelTextField.text = dict?.vehicleModelName
            brandTextField.text = dict?.vehicleBrandName
            mileageTextField.text = dict?.mileage
            colorTextField.text = dict?.color
            modelYearTextField.text = dict?.vehicleModelYear
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editBtnClicked(_ sender: Any) {
    }

    //MARK: Textfiled Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
