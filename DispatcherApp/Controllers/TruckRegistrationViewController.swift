//
//  TruckRegistrationViewController.swift
//  DispatcherApp
//
//  Created by Webwerks on 16/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD

class TruckRegistrationViewController: UIViewController,FSCalendarDataSource,FSCalendarDelegate,UITableViewDataSource,UITableViewDelegate {

    var selectedBtn  :UIButton!
    var dateString : String?
    var arrayOfServices : [AnyObject]?
    var selectedTruck : truckListObj?
    var isFromTruckList : Bool?
    
    
    @IBOutlet weak var customCalenderView: FSCalendar!
    @IBOutlet weak var servicesTableView: UITableView!
    @IBOutlet weak var startingDutiesDateBtn: UIButton!
    @IBOutlet weak var registrationBtn: UIButton!
    @IBOutlet weak var regExpiryBtn: UIButton!
    @IBOutlet weak var policyIssuedBtn: UIButton!
    @IBOutlet weak var policyExpBtn: UIButton!
    @IBOutlet weak var modelYearBtn: UIButton!
    @IBOutlet weak var purchaseDateBtn: UIButton!
    @IBOutlet var calenderView: UIView!
    @IBOutlet weak var truckCodeTextField: UITextField!
    @IBOutlet weak var truckNickNameTextField: UITextField!
    @IBOutlet weak var plateNoTextField: UITextField!
    @IBOutlet weak var plateCodeTextField: UITextField!
    @IBOutlet weak var vinTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var modelTextField: UITextField!
    @IBOutlet weak var brandTextField: UITextField!
    @IBOutlet weak var insuranceCompanyTextField: UITextField!
    @IBOutlet weak var policyNoTextField: UITextField!
    
    @IBOutlet weak var sideMenuBtn: UIButton!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var updateBtn: UIButton!
    
    @IBOutlet weak var backToListBtn: UIButton!
    
    //MARK: View Method
 override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
       arrayOfServices = []
    print("selected truck\(selectedTruck)")
   
    if(isFromTruckList == true)
    {
        self.setupTruckDetail()
       sideMenuBtn.setImage(UIImage(named: "back"), for: .normal)
        submitBtn.isHidden = true
        updateBtn.isHidden = false
        backToListBtn.isHidden = false
    }
    else
    {
        submitBtn.isHidden = false
        updateBtn.isHidden = true
        backToListBtn.isHidden = true
        self.callGetServiceAndBrandWS()
    }
}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func setupTruckDetail() {
        truckCodeTextField.text = selectedTruck?.truckCode
        truckNickNameTextField.text = selectedTruck?.truckNickName
        plateNoTextField.text = selectedTruck?.plateNumber
        plateCodeTextField.text = selectedTruck?.plateCode
        vinTextField.text = selectedTruck?.vin
        brandTextField.text = selectedTruck?.truckBrandID?.description
        modelTextField.text = selectedTruck?.truckModelID?.description
       policyNoTextField.text = selectedTruck?.truckPolicyNumber
        colorTextField.text = selectedTruck?.truckColor
        insuranceCompanyTextField.text = selectedTruck?.insCompanyName
        
        purchaseDateBtn.setTitle(selectedTruck?.dateOfPurchase, for: .normal)
        startingDutiesDateBtn.setTitle(selectedTruck?.dateOfAcquiring, for: .normal)
        registrationBtn.setTitle(selectedTruck?.registrationDate, for: .normal)

        policyIssuedBtn.setTitle(selectedTruck?.truckPolicyIssuedDate, for: .normal)
        policyExpBtn.setTitle(selectedTruck?.truckPolicyExpDate, for: .normal)
        regExpiryBtn.setTitle(selectedTruck?.truckReRegDueDate, for: .normal)
        arrayOfServices = selectedTruck?.allservicesList as [AnyObject]?
        modelYearBtn.setTitle(selectedTruck?.modelYear, for: .normal)

        self.servicesTableView.reloadData()
       
    }
    
    
    //MARK: WS for service and brand
    func callGetServiceAndBrandWS() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let GetServiceParams: [String: AnyObject] = ["clientId":"8" as AnyObject]
        
        WebserviceHelper() .callGetServiceAndBrandAPI(parameters: GetServiceParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responce = ServicesAndBrand(JSONString : response)
            if responce?.isSuccess == true
            {
                if let data = responce?.services{
                    self.arrayOfServices = data as [AnyObject]
                    
                    DispatchQueue.main.async {
                        self.servicesTableView.reloadData()
                    }
                }
            }
            else
            {
                CommonMethod().alertMessage("Status", message:"Server error", currentVC: self)
            }
            
        }
        }

    //MARK: TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.arrayOfServices!.count)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TruckServicesCell = tableView.dequeueReusableCell(withIdentifier: "TruckServicesCell") as! TruckServicesCell!
        
        let object = self.arrayOfServices?[indexPath.row]
        
        if object is ServicesArray {
            if let services : ServicesArray = object as? ServicesArray
                
            {
                cell.servicesLabel.text = services.description
                if(services.checked == true)
                {
                    cell.serviceBtn.isSelected = true
                }
                else
                {
                    cell.serviceBtn.isSelected = false
                }
            }
            
            cell.serviceBtn?.addTarget(self, action: #selector(serviceBtnClicked), for: .touchUpInside)
            return cell;
        }
        else{
            if let services : allServicesData = object as? allServicesData
                
            {
                cell.servicesLabel.text = services.description
                if(services.checked == true)
                {
                    cell.serviceBtn.isSelected = true
                }
                else
                {
                    cell.serviceBtn.isSelected = false
                }
            }
            
            cell.serviceBtn?.addTarget(self, action: #selector(serviceBtnClicked), for: .touchUpInside)
            return cell;
        }
      }
    

    //MARK: WS for RegisterTruck
    func callRegisterTruckWS() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
       
        var service:[AnyObject]? = nil
        if let arrayOfServices = arrayOfServices{
        service = arrayOfServices
        }
        
        var tempArray:[[String: Any]] = []
        
        for object in service!{
            let temp = object as! ServicesArray
            let tempObject = temp.toJSON()
            tempArray.append(tempObject)
        }
    
//   let truckRegisterParams: [String: AnyObject] = ["truckCode":"TRUCKCODE1" as AnyObject, "plateCode":"PLC3433443" as AnyObject,"plateNumber":"PLT232332332" as AnyObject,"vin":"VIN23434343434344" as AnyObject,"truckColor":"BLACK" as AnyObject,"insCompanyName":"INS 54544544" as AnyObject,"truckPolicyNumber":"PLCY45554545" as AnyObject,"truckNickName":"TRUCKNICKNAME" as AnyObject,"dateOfPurchase":"2017-05-19T10:24:56.882Z" as AnyObject,"registrationDate":"2017-05-19T10:24:56.883Z" as AnyObject,"dateOfAcquiring":"2017-05-19T10:24:56.883Z" as AnyObject,"modelYear":"2016-12-31T18:30:00.000Z" as AnyObject,"truckPolicyIssuedDate":"2017-05-19T10:24:56.883Z" as AnyObject,"truckPolicyExpiryDate":"2018-06-19T10:24:56.883Z" as AnyObject,"truckReRegistrationDueDate":"2018-05-19T10:24:56.883Z" as AnyObject,"clientId":"1" as AnyObject,"timeZone":"Asia/Dubai" as AnyObject,"userId":"572" as AnyObject,"truckBrandID":1 as AnyObject,"truckModelID":2 as AnyObject,"isFirst":true as AnyObject,"servicesProvided":tempArray as AnyObject]

        
        let truckRegisterParams:[String :AnyObject] = ["truckCode":truckCodeTextField.text as AnyObject, "plateCode":plateCodeTextField.text as AnyObject,"plateNumber":plateNoTextField.text as AnyObject,"vin":vinTextField.text as AnyObject,"truckColor":colorTextField.text as AnyObject,"insCompanyName":insuranceCompanyTextField.text as AnyObject,"truckPolicyNumber":policyNoTextField.text as AnyObject,"truckNickName":truckNickNameTextField.text as AnyObject,"dateOfPurchase":purchaseDateBtn.titleLabel?.text as AnyObject,"registrationDate":registrationBtn.titleLabel?.text as AnyObject,"dateOfAcquiring":startingDutiesDateBtn.titleLabel?.text as AnyObject,"modelYear":modelYearBtn.titleLabel?.text as AnyObject,"truckPolicyIssuedDate":policyIssuedBtn.titleLabel?.text as AnyObject,"truckPolicyExpiryDate":policyExpBtn.titleLabel?.text as AnyObject,"truckReRegistrationDueDate":regExpiryBtn.titleLabel?.text as AnyObject,"clientId":"1" as AnyObject,"timeZone":"Asia/Dubai" as AnyObject,"userId":"572" as AnyObject,"truckBrandID":brandTextField.text as AnyObject,"truckModelID":modelTextField.text as AnyObject,"isFirst":true as AnyObject,"servicesProvided":tempArray as AnyObject]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: truckRegisterParams,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        
        WebserviceHelper() .calltruckRegisterAPI(parameters: truckRegisterParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let data = response.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    let serviveMsg = (json as! NSDictionary)["serviceMessage"] as! String
                    CommonMethod().alertMessage("Status", message:serviveMsg as NSString , currentVC: self)
                } catch {
                    CommonMethod().alertMessage("Status", message:"Parse error" as NSString , currentVC: self)
                }
            }
      }
    }
    
    
    
    //MARK: Button Action

    func serviceBtnClicked(sender:UIButton ) {

        if let cell = sender.superview?.superview as? TruckServicesCell
        {
            let indexPath = self.servicesTableView.indexPath(for: cell)
            
            var temp  = (self.arrayOfServices![indexPath!.row])
            
            if temp is ServicesArray {
            var servicesBrand : ServicesArray = (temp as? ServicesArray)!
                
        if(cell.serviceBtn.isSelected == true)
            {
                cell.serviceBtn.isSelected = false
                servicesBrand.checked = false
            }
            else
            {
                cell.serviceBtn.isSelected = true
                servicesBrand.checked = true
                }
        temp = servicesBrand as AnyObject
            }
            else
            {
                    var servicesFromList : allServicesData = (temp as? allServicesData)!
               
                    if(cell.serviceBtn.isSelected == true)
                    {
                        cell.serviceBtn.isSelected = false
                        servicesFromList.checked = false
                    }
                    else
                    {
                        cell.serviceBtn.isSelected = true
                        servicesFromList.checked = true
                    }
                temp = servicesFromList as AnyObject
            }
            self.arrayOfServices?[(indexPath?.row)!] = temp
             }
             }
    
    
    
    @IBAction func submitBtnClick(_ sender: UIButton) {
        
        if(!truckCodeTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter truck code", currentVC: self)
            return
         }
      else if(!truckNickNameTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter truck nick name", currentVC: self)
            return
        }
        else if(!plateNoTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter plate Number", currentVC: self)
            return
        }
        else if(!plateCodeTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter plate code", currentVC: self)
            return
        }
        else if(!vinTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter VIN", currentVC: self)
            return
        }
        else if(!colorTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter color", currentVC: self)
            return
        }
        else if(!brandTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter brand", currentVC: self)
            return
        }
        else if(!modelTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter model No", currentVC: self)
            return
        }

        else
        {
            self.callRegisterTruckWS()
         }
    }

    @IBAction func startingDutiesDateBtnClick(_ sender: UIButton) {
        selectedBtn = sender
        
        if !self.view.subviews.contains(self.calenderView) {
            calenderView.frame = CGRect(x: 0, y: 0, width: self.calenderView.frame.size.width, height: self.calenderView.frame.size.height)
            self.view.addSubview(calenderView)
            self.calenderView.center = self.view.center
            self.customCalenderView.delegate = self
        }
    }
    
    @IBAction func registrationBtnClick(_ sender: UIButton) {
        selectedBtn = sender
        if !self.view.subviews.contains(self.calenderView) {
            
            calenderView.frame = CGRect(x: 0, y: 0, width: self.calenderView.frame.size.width, height: self.calenderView.frame.size.height)
            self.view.addSubview(calenderView)
            self.calenderView.center = self.view.center
            self.customCalenderView.delegate = self
        }
    }
    
    @IBAction func regExpiryBtnClick(_ sender: UIButton) {
        selectedBtn = sender
        
        if !self.view.subviews.contains(self.calenderView) {
            
            calenderView.frame = CGRect(x: 0, y: 0, width: self.calenderView.frame.size.width, height: self.calenderView.frame.size.height)
            self.view.addSubview(calenderView)
            self.calenderView.center = self.view.center
            self.customCalenderView.delegate = self
        }
    }
    
    
    @IBAction func policyIssuedBtnClick(_ sender: UIButton) {
        selectedBtn = sender
        
        if !self.view.subviews.contains(self.calenderView) {
            
            calenderView.frame = CGRect(x: 0, y: 0, width: self.calenderView.frame.size.width, height: self.calenderView.frame.size.height)
            self.view.addSubview(calenderView)
            self.calenderView.center = self.view.center
            self.customCalenderView.delegate = self
        }
}
    
    @IBAction func policyExpBtnClick(_ sender: UIButton) {
        selectedBtn = sender
        
        if !self.view.subviews.contains(self.calenderView) {
            
            calenderView.frame = CGRect(x: 0, y: 0, width: self.calenderView.frame.size.width, height: self.calenderView.frame.size.height)
            self.view.addSubview(calenderView)
            self.calenderView.center = self.view.center
            self.customCalenderView.delegate = self
        }
    }
    
    @IBAction func modelYearBtnClicked(_ sender: UIButton) {
        selectedBtn = sender
        
        if !self.view.subviews.contains(self.calenderView) {
            
            calenderView.frame = CGRect(x: 0, y: 0, width: self.calenderView.frame.size.width, height: self.calenderView.frame.size.height)
            self.view.addSubview(calenderView)
            self.calenderView.center = self.view.center
            self.customCalenderView.delegate = self
        }
    }
    
    @IBAction func purchaseDateBtnClick(_ sender: UIButton) {
        selectedBtn = sender
        
        if !self.view.subviews.contains(self.calenderView) {
            
            calenderView.frame = CGRect(x: 0, y: 0, width: self.calenderView.frame.size.width, height: self.calenderView.frame.size.height)
            self.view.addSubview(calenderView)
            self.calenderView.center = self.view.center
            self.customCalenderView.delegate = self
        }
    }

    
    @IBAction func cancelCalnderBtnClick(_ sender: UIButton) {
        if self.view.subviews.contains(self.calenderView) {
        self.calenderView.removeFromSuperview()
        }
    }
    
    @IBAction func okCalenderBtnClick(_ sender: UIButton) {
        selectedBtn.setTitle(dateString, for: .normal)

        if self.view.subviews.contains(self.calenderView) {
            self.calenderView.removeFromSuperview()
        }
    }
    
    @IBAction func updateBtnClicked(_ sender: UIButton) {
      self.callUpdateTruckWS()
    }
    
    
    @IBAction func backToListBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    
   //MARK: SideMenuBtn Clicked
    @IBAction func sideMenuBtnClicked(_ sender: UIButton) {
        if(isFromTruckList == true)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
        slideMenuController()?.toggleLeft()
        }
    }
    
   
    //MARK: WS for RegisterTruck
    func callUpdateTruckWS() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var service:[AnyObject]? = nil
        if let arrayOfServices = arrayOfServices{
            service = arrayOfServices
        }
        
        var tempUpdateArray:[[String: Any]] = []
        
        for object in service!{
            let temp = object as! allServicesData
            let tempObject = temp.toJSON()
            tempUpdateArray.append(tempObject)
        }
        
        //   let updateTruckParams: [String: AnyObject] = ["truckCode":"TRUCKCODE1" as AnyObject, "plateCode":"PLC3433443" as AnyObject,"plateNumber":"PLT232332332" as AnyObject,"vin":"VIN23434343434344" as AnyObject,"truckColor":"BLACK" as AnyObject,"insCompanyName":"INS 54544544" as AnyObject,"truckPolicyNumber":"PLCY45554545" as AnyObject,"truckNickName":"TRUCKNICKNAME" as AnyObject,"dateOfPurchase":"2017-05-19T10:24:56.882Z" as AnyObject,"registrationDate":"2017-05-19T10:24:56.883Z" as AnyObject,"dateOfAcquiring":"2017-05-19T10:24:56.883Z" as AnyObject,"modelYear":"2016-12-31T18:30:00.000Z" as AnyObject,"truckPolicyIssuedDate":"2017-05-19T10:24:56.883Z" as AnyObject,"truckPolicyExpiryDate":"2018-06-19T10:24:56.883Z" as AnyObject,"truckReRegistrationDueDate":"2018-05-19T10:24:56.883Z" as AnyObject,"clientId":"1" as AnyObject,"timeZone":"Asia/Dubai" as AnyObject,"userId":"572" as AnyObject,"truckBrandID":1 as AnyObject,"truckModelID":2 as AnyObject,"isFirst":true as AnyObject,"servicesProvided":tempUpdateArray as AnyObject]
        
        
        let updateTruckParams:[String :AnyObject] = ["truckCode":truckCodeTextField.text as AnyObject,
        "plateCode":plateCodeTextField.text as AnyObject,
        "plateNumber":plateNoTextField.text as AnyObject,
        "vin":vinTextField.text as AnyObject,
        "truckColor":colorTextField.text as AnyObject,
        "insCompanyName":insuranceCompanyTextField.text as AnyObject,
        "truckPolicyNumber":policyNoTextField.text as AnyObject,
        "truckNickName":truckNickNameTextField.text as AnyObject,
        "dateOfPurchase":purchaseDateBtn.titleLabel?.text as AnyObject,
        "registrationDate":registrationBtn.titleLabel?.text as AnyObject,
        "dateOfAcquiring":startingDutiesDateBtn.titleLabel?.text as AnyObject,
        "modelYear":modelYearBtn.titleLabel?.text as AnyObject,
        "truckPolicyIssuedDate":policyIssuedBtn.titleLabel?.text as AnyObject,
        "truckPolicyExpiryDate":policyExpBtn.titleLabel?.text as AnyObject,
        "truckReRegistrationDueDate":regExpiryBtn.titleLabel?.text as AnyObject,
        "clientId":"1" as AnyObject,
        "timeZone":"Asia/Dubai" as AnyObject,
        "userId":"572" as AnyObject,
        "truckBrandID":brandTextField.text as AnyObject,
        "truckModelID":modelTextField.text as AnyObject,
        "availableHoursPerDay":"4" as AnyObject,
        "truckId":selectedTruck?.truckId as AnyObject,
        "servicesProvided":tempUpdateArray as AnyObject]
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: updateTruckParams,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        
        WebserviceHelper() .callUpdateTruckAPI(parameters: updateTruckParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let data = response.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    let serviveMsg = (json as! NSDictionary)["serviceMessage"] as! String
                    CommonMethod().alertMessage("Status", message:serviveMsg as NSString , currentVC: self)
                } catch {
                    CommonMethod().alertMessage("Status", message:"Parse error" as NSString , currentVC: self)
                }
            }
        }
    }
    
    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Dismiss KeyBoard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    //MARK: FSCalender Delegate
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
            
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateString = dateFormatter.string(from: date)
        
    }
    func setDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
         dateString = dateFormatter.string(from: date)
        return dateString!
    }
}



