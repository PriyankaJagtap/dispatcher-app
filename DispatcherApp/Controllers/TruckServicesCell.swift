//
//  TruckServicesCell.swift
//  DispatcherApp
//
//  Created by Webwerks on 19/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class TruckServicesCell: UITableViewCell {

    
    @IBOutlet weak var serviceBtn: UIButton!
    
    @IBOutlet weak var servicesLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
