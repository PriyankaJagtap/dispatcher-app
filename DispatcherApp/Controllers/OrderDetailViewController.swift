//
//  OrderDetailViewController.swift
//  YummyFoodTrackIn
//
//  Created by webwerks on 3/20/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

import UIKit
import MBProgressHUD

class OrderDetailViewController: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBAction func backBtnClick(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.getCaseDetails()
        pageControl.currentPage = 0
        pageControl.addTarget(self, action: #selector(self.didChangePageControlValue), for: .valueChanged)
        self.title = "Order Details"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTapped(sender: UIBarButtonItem) {
        
        self .dismiss(animated: true, completion: nil)

    }
    
    var tutorialPageViewController: PageViewController? {
        didSet {
            tutorialPageViewController?.tutorialDelegate = self
        }
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? PageViewController {
            tutorialPageViewController.tutorialDelegate = self
            self.tutorialPageViewController = tutorialPageViewController;
        }
    }
    
    func didChangePageControlValue() {
        tutorialPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }
   
//    func getCaseDetails()
//    {
//        MBProgressHUD.showAdded(to: self.view, animated: true)
//        let loginParams: [String: AnyObject] = ["userId" : "572" as AnyObject, "clientId" : "1" as AnyObject, "searchBy" : "5498" as AnyObject, "isCustomer" : true as AnyObject ,"isById" : true as AnyObject, "timeZone" : "Asia/Dubai" as AnyObject]
//        
//        WebserviceHelper() .callGetCaseDetailsAPi(parameters: loginParams as! NSMutableDictionary) { (response) in
//
//            MBProgressHUD.hide(for: self.view, animated: true)
//            let responceMsg = CaseDetails(JSONString : response)
//            if responceMsg?.isSuccess == true{
//                CaseDetailsSharedData.sharedInstance.casedetail = responceMsg!
//                print("success")
//              let caseDict = responceMsg?.caseDetailsDictionary
//                let street = caseDict?.street
//                
//                
//                               }
//            else
//            {
//                //                CommonMethod().alertMessage("Status", message:"Please try again" as NSString , currentVC: self)
//                print("error")
//                
//            }
//            
//        }
//    }
    

}

extension OrderDetailViewController: OrderPageViewControllerDelegate {
    
    func tutorialPageViewController(tutorialPageViewController: PageViewController,
                                    didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func tutorialPageViewController(tutorialPageViewController: PageViewController,
                                    didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
}
