//
//  ContactDetailCollectionCell.swift
//  DispatcherApp
//
//  Created by Webwerks on 27/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class ContactDetailCollectionCell: UICollectionViewCell,UITextFieldDelegate {

    @IBOutlet weak var driverNameTextField: UITextField!
    
    @IBOutlet weak var areaCodeTextField: UITextField!
    @IBOutlet weak var countryCodeTextField: UIButton!
    
    
    @IBOutlet weak var btnCountryCode: UIButton!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var buildingTextField: UITextField!
    @IBOutlet weak var landmarkTextField: UITextField!
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var postBoxTextField: UITextField!
    
    @IBOutlet weak var countryTextField: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
