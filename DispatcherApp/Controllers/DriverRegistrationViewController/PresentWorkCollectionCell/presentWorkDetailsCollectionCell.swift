//
//  presentWorkDetailsCollectionCell.swift
//  DispatcherApp
//
//  Created by Sonali on 6/27/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class presentWorkDetailsCollectionCell: UICollectionViewCell,UITextFieldDelegate {
   @IBOutlet weak var nameTextField: UITextField!
     @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userTypeTextField: UITextField!
    @IBOutlet weak var empIDtextField: UITextField!
    @IBOutlet weak var btnDOJ: UIButton!
    @IBOutlet weak var designationTextField: UITextField!
    @IBOutlet weak var btnWorkingHours: UIButton!
    @IBOutlet weak var btnPreferredOffDay: UIButton!
    @IBOutlet weak var btnReportingTo: UIButton!
    @IBOutlet weak var btnShirtSize: UIButton!
    @IBOutlet weak var btnTrouserSize: UIButton!
    @IBOutlet weak var btnShoesSize: UIButton!

    @IBOutlet weak var btnNoOfShirtReceived: UIButton!
    
    @IBOutlet weak var btnNoOfTrouserReceived: UIButton!
    
   
    @IBOutlet weak var btnNoOfShoesReceived: UIButton!
   
    @IBOutlet weak var btnSelectVehicle: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
