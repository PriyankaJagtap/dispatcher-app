//
//  ExperienceTableViewCell.swift
//  DispatcherApp
//
//  Created by Sonali on 6/26/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var totalExpTextField: UITextField!
    @IBOutlet weak var relatedExpTextField: UITextField!
    @IBOutlet weak var designationTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var btnDateOfJoining: UIButton!
    @IBOutlet weak var btnDateOfRelieving: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
}
