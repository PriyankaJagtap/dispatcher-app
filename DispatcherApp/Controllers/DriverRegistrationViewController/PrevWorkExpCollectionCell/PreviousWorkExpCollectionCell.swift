//
//  PreviousWorkExpCollectionCell.swift
//  DispatcherApp
//
//  Created by Webwerks on 27/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class PreviousWorkExpCollectionCell: UICollectionViewCell,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CustomCalenderViewDelegate{

    @IBOutlet weak var workExpTableView: UITableView!
    var experiencearray = [[String : AnyObject]]()
    var experienceDict = [String: AnyObject]()
    
    var selectedBtn  :UIButton!
   var customCalenderPopUp : CustomCalendarView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //experiencearray.append("new")

        workExpTableView.register(UINib(nibName: "ExperienceTableViewCell",
                                           bundle: nil),
                                     forCellReuseIdentifier: "cellExprience")
        
        workExpTableView.delegate = self
        workExpTableView.dataSource = self
        workExpTableView.tableFooterView = UIView(frame: .zero)

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (experiencearray.count)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:ExperienceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellExprience") as! ExperienceTableViewCell!
    
        cell.btnDelete.addTarget(self, action: #selector(deleteCompanyDetails(sender:)), for: .touchUpInside)
        cell.btnDateOfJoining.addTarget(self, action: #selector(companyJoiningDetails(sender:)), for: .touchUpInside)
        cell.btnDateOfRelieving.addTarget(self, action: #selector(companyRelievingDetails(sender:)), for: .touchUpInside)
        
         experienceDict["totalWorkExperience"] = cell.totalExpTextField.text as AnyObject?
         experienceDict["relatedWorkExperience"] = cell.relatedExpTextField.text as AnyObject?
         experienceDict["companyName"] = cell.companyNameTextField.text as AnyObject?
         experienceDict["designation"] = cell.designationTextField.text as AnyObject?
         experienceDict["dateOfJoining"] = cell.btnDateOfJoining.titleLabel?.text as AnyObject?
         experienceDict["dateOfRelieving"] = cell.btnDateOfRelieving.titleLabel?.text as AnyObject?
        
        return cell
    }

    func deleteCompanyDetails(sender : UIButton)  {
        
        let pointInTable: CGPoint =   sender.convert(sender.bounds.origin, to: workExpTableView)
        
        let cellIndexPath = workExpTableView.indexPathForRow(at: pointInTable)
        experiencearray.remove(at: (cellIndexPath?.row)!)
        
        workExpTableView.reloadData()
    }
    
    
    func companyJoiningDetails(sender : UIButton)
    {
       selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        self.window?.addSubview(customCalenderPopUp!)
        customCalenderPopUp?.delegate = self
        
        if !(self.window?.subviews.contains(customCalenderPopUp!))!
        {
            self.window?.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
         }
    }
    
    func companyRelievingDetails(sender : UIButton)
    {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        self.window?.addSubview(customCalenderPopUp!)
        customCalenderPopUp?.delegate = self
        
        if !(self.window?.subviews.contains(customCalenderPopUp!))!
        {
            self.window?.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
            }
    }
    
    @IBAction func oneMoreCompanyBtnClicked(_ sender: UIButton) {
        experiencearray.append(experienceDict)
        workExpTableView.reloadData()
    }
    
    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Custom Calenar View Delegate
    func btnOkClicked(_ sender: UIButton, datestring:String)
    {
        selectedBtn.setTitle(datestring, for: .normal)
        if (self.window?.subviews.contains(customCalenderPopUp!))!
        {
            customCalenderPopUp?.removeFromSuperview()
        }
    }
    
    func btnCancelClicked(_ sender: UIButton)
    {
        if (self.window?.subviews.contains(customCalenderPopUp!))! {
           customCalenderPopUp?.removeFromSuperview()
        }
        
    }
}
