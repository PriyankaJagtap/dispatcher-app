//
//  DriverDetailCollectionCell.swift
//  DispatcherApp
//
//  Created by Webwerks on 27/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class DriverDetailCollectionCell: UICollectionViewCell,UITextFieldDelegate{

   //TextField Outlet
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var licenseNoTextField: UITextField!
    @IBOutlet weak var govtIDTextField: UITextField!
    @IBOutlet weak var nationalityTextField: UITextField!
    @IBOutlet weak var passportTextField: UITextField!
    @IBOutlet weak var viseTextField: UITextField!

    //Button Outlet
   @IBOutlet weak var btnLicenceExpiryDate: UIButton!
    @IBOutlet weak var insCardNoTextField: UITextField!
    @IBOutlet weak var btnPassportIssuedDate: UIButton!
    @IBOutlet weak var btnPassportExpiryDate: UIButton!
    @IBOutlet weak var btnVisaIssuedDate: UIButton!
    @IBOutlet weak var btnVisaExpDate: UIButton!
    @IBOutlet weak var btnInsCardExpDate: UIButton!
    

   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    
}
