//
//  DriversRegistrationViewController.swift
//  DispatcherApp
//
//  Created by Webwerks on 27/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD

class DriversRegistrationViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CustomCalenderViewDelegate{
    
    @IBOutlet weak var driverCollectionView: UICollectionView!
    let driverDetailCellIdentifier = "DriverDetailCellIdentifier"
    let contactCellIdentifier = "ContactDetailCellIdentifier"
    let emergencyContactCellIdentifier = "EmergencyContactCellIdentifier"
    let presentWorkCellIdentifier = "presentWorkDetailsCellIdentifier"
    let prevWorkExpCellIdentifier = "PreviousWorkExpCellIdentifier"
    var selectedBtn  :UIButton!
    var driverDetailDict = [String: AnyObject]()
    var contactDetailDict = [String: AnyObject]()
    var emergencyContactDetailDict = [String: AnyObject]()
    var prevExpDetailArray = [[String: AnyObject]]()
    var expDict = [String: AnyObject]()
    var regDriverParams =  [String:AnyObject]()
    
    var dropDown : NIDropDown?
    var shiftListArray : [ShiftListObj]?
    var managerListArray : [ManagersObj]?
    var truckListArray : [TruckListAndCodeObj]?
     var daysArray:[[String: Any]] = []
    
    var selectedShiftID : Int = 0
    var selectedDayOffID : Int = 0
    var selectedManagerID : Int = -1
    var selectedShirtSizeID : Int = 0
    var selectedTrouserSizeID : Int = 0
    var selectedShoesSizeID : Int = 0
    var selectedNoOfShirtID : Int = 0
    var selectedNoOfTrouserID : Int = 0
    var selectedNoOfShoesID : Int = 0
    var selectedVehicleID : Int = 0
    
    var countryNameArray : [CountriesArray]? = []
    @IBOutlet weak var btnSaveDetail: UIButton!
    @IBOutlet weak var btnCancelDetail: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    
    @IBOutlet weak var sidemenuBtn: UIButton!
    var customCalenderPopUp : CustomCalendarView?
   
    // to display driver detail
    var isFromDriverList:Bool?
    var DriverId : Int = 0
    var driverExperineceArray : [DriverExpList]?
    var driverInfoDict : DriverInfoData?
    
    
    //MARK:
    //MARK: View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.setupOfCollectionView()
        shiftListArray = [ShiftListObj]()
        managerListArray = [ManagersObj]()
        truckListArray = [TruckListAndCodeObj]()
        
        daysArray = [["dayName":"Sunday","dayId":"1"],["dayName":"Monday","dayId":"2"],["dayName":"Tuesday","dayId":"3"],["dayName":"Wednesday","dayId":"4"],["dayName":"Thursday","dayId":"5"],["dayName":"Friday","dayId":"6"],["dayName":"Saturday","dayId":"7"]]

        if(isFromDriverList == true)
        {
            sidemenuBtn.setImage(UIImage(named: "back"), for: .normal)
            self.callGetDriverDetailAPI()
        }
       
        
        self.callGetProblemServicesAPI()

        
       // for object in (SharedInstanceClass.sharedInstance.problemServices?.countriesArray)!{
         //   countryNameArray.append(object.countryCodeName!)
       // }
      // print(countryNameArray)
        
        self.CallTrucksAndShiftManagerWS()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        btnSaveDetail.isHidden = true
        btnCancelDetail.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Side Menu Button Clicked
    @IBAction func sideMenuBtnClicked(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
    }
    
    //MARK:
    //MARK: CollectionView Setup
    func setupOfCollectionView() {
        driverCollectionView.register(UINib(nibName:"DriverDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: driverDetailCellIdentifier)
        driverCollectionView.register(UINib(nibName:"ContactDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: contactCellIdentifier)
        driverCollectionView.register(UINib(nibName:"EmergencyContactCollectionCell", bundle: nil), forCellWithReuseIdentifier: emergencyContactCellIdentifier)
        driverCollectionView.register(UINib(nibName:"presentWorkDetailsCollectionCell", bundle: nil), forCellWithReuseIdentifier: presentWorkCellIdentifier)
        driverCollectionView.register(UINib(nibName:"PreviousWorkExpCollectionCell", bundle: nil), forCellWithReuseIdentifier: prevWorkExpCellIdentifier)
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.itemSize = CGSize(width: CGFloat(view.frame.size.width), height: CGFloat(driverCollectionView.frame.size.height))
        
        driverCollectionView.collectionViewLayout = flowLayout
        driverCollectionView.delegate = self
        driverCollectionView.dataSource = self
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    //MARK: CallTrucksAndShiftManagerWS
    func CallTrucksAndShiftManagerWS() {
        
        let jobShift: [String: AnyObject] =  ["clientId" : UserDefault.clientID as AnyObject]
        
        WebserviceHelper() .callTruckAndShiftManagerAPI(parameters:jobShift as! NSMutableDictionary) { (response) in
            
            let responce = TruckAndShiftManager(JSONString : response)
           
            if responce?.isSuccess == true
            {
                if (responce?.shiftList) != nil{
                    if let data = responce?.shiftList{
                        self.shiftListArray = data
                    }
                  }
              
                if (responce?.managers) != nil{
                    if let data = responce?.managers{
                        self.managerListArray = data
                    }
                }
                
                if (responce?.truckList) != nil{
                    if let data = responce?.truckList{
                        self.truckListArray = data
                    }
                }
                }
           
        }
}
  
    //MARK: CallGetProblemServices

    func callGetProblemServicesAPI() -> Void {
       // MBProgressHUD.showAdded(to: self.view, animated: true)
        let addJobParams: [String: AnyObject] = ["portalId" : UserDefault.clientID as AnyObject]
        
        WebserviceHelper() .callGetProblemServicesAPI(parameters: addJobParams as! NSMutableDictionary) { (response) in
            
           // MBProgressHUD.hide(for: self.view, animated: true)
            let responce = ProblemServices(JSONString : response)
            
            if responce?.isSuccess == true{
                print("success")
                
                if (responce?.countriesArray) != nil{
                    if let data = responce?.countriesArray{
                        self.countryNameArray = data
                    }
                }
                
               }
            else
            {
                CommonMethod().alertMessage("Status", message:"Please try again" as NSString , currentVC: self)
                print("error")
                
            }
            
        }
    }

    //MARK: CallGetDriverDetailWS
    
    func callGetDriverDetailAPI() -> Void {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let addDriverParams: [String: AnyObject] = ["driverId" : DriverId as AnyObject]
        
        WebserviceHelper() .callDriverDetailAPI(parameters: addDriverParams as! NSMutableDictionary) { (response) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
           let responce = DriverDetail(JSONString : response)
            
           if responce?.isSuccess == true{
                print("success")
            
              if (responce?.driverExpList) != nil{
                  if let data = responce?.driverExpList{
                       self.driverExperineceArray = data
                    }
                }
            
         
                if let dataDict = responce?.driverInfoDict{
                    self.driverInfoDict = dataDict
                }
        
            
            DispatchQueue.main.async {
                self.driverCollectionView.reloadData()
            }
            
                }
           else
            {
                CommonMethod().alertMessage("Status", message:"Please try again" as NSString , currentVC: self)
                print("error")
                
            }
            
        }
    }

    
    //MARK: CollectionView Datasource & Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        
        if indexPath.row == 0 {
            let detailCell = collectionView.dequeueReusableCell(withReuseIdentifier: driverDetailCellIdentifier, for: indexPath) as! DriverDetailCollectionCell
            
            if(isFromDriverList == true) {
                detailCell.firstNameTextField.isEnabled = false
                detailCell.lastNameTextField.isEnabled = false
                 detailCell.licenseNoTextField.isEnabled = false
                 detailCell.govtIDTextField.isEnabled = false
                 detailCell.nationalityTextField.isEnabled = false
                 detailCell.passportTextField.isEnabled = false
                 detailCell.viseTextField.isEnabled = false
                detailCell.insCardNoTextField.isEnabled = false
                detailCell.btnPassportIssuedDate.isEnabled = false
                detailCell.btnPassportExpiryDate.isEnabled = false
                detailCell.btnLicenceExpiryDate.isEnabled = false
                detailCell.btnVisaExpDate.isEnabled = false
                detailCell.btnVisaIssuedDate.isEnabled = false
                detailCell.btnInsCardExpDate.isEnabled = false

                detailCell.firstNameTextField.text = driverInfoDict?.driverFirstName
                detailCell.lastNameTextField.text = driverInfoDict?.driverLastName
                detailCell.licenseNoTextField.text = driverInfoDict?.licenseNo
                detailCell.govtIDTextField.text = driverInfoDict?.governmentIdNumber
                detailCell.nationalityTextField.text = driverInfoDict?.nationality
                detailCell.passportTextField.text = driverInfoDict?.passportNo
                detailCell.viseTextField.text = driverInfoDict?.visaNo
                detailCell.insCardNoTextField.text = driverInfoDict?.healthInsuranceCardNo
                detailCell.btnLicenceExpiryDate.titleLabel?.text = driverInfoDict?.licenseNoExpiredAt
                detailCell.btnPassportIssuedDate.titleLabel?.text = driverInfoDict?.passportIssuedAt
                detailCell.btnPassportExpiryDate.titleLabel?.text = driverInfoDict?.passportExpiredAt
                detailCell.btnVisaExpDate.titleLabel?.text = driverInfoDict?.visaExpiredAt
                detailCell.btnVisaIssuedDate.titleLabel?.text = driverInfoDict?.visaIssuedAt
                detailCell.btnInsCardExpDate.titleLabel?.text = driverInfoDict?.healthInsuranceCardExpiryAt
            }
        else
            {
            detailCell.btnLicenceExpiryDate.addTarget(self, action: #selector(btnLicenceExpiryDateClicked(sender:)), for: .touchUpInside)
           
                detailCell.btnPassportIssuedDate.addTarget(self, action: #selector(btnPassportIssuedDateClicked(sender:)), for: .touchUpInside)
            
            detailCell.btnPassportExpiryDate.addTarget(self, action: #selector(btnPassportExpiryDateClicked(sender:)), for: .touchUpInside)
            
            detailCell.btnVisaIssuedDate.addTarget(self, action: #selector(btnVisaIssuedDateClicked(sender:)), for: .touchUpInside)
            
            detailCell.btnVisaExpDate.addTarget(self, action: #selector(btnVisaExpDateClicked(sender:)), for: .touchUpInside)
            
            detailCell.btnInsCardExpDate.addTarget(self, action: #selector(btnInsCardExpDateClicked(sender:)), for: .touchUpInside)
            
            }
            
            return detailCell
        }
        if indexPath.row == 1 {
            let contactCell = collectionView.dequeueReusableCell(withReuseIdentifier: contactCellIdentifier, for: indexPath) as! ContactDetailCollectionCell
            
            if(isFromDriverList == true) {
                contactCell.driverNameTextField.isEnabled = false
                contactCell.areaCodeTextField.isEnabled = false
                contactCell.phoneTextField.isEnabled = false
                contactCell.emailTextField.isEnabled = false
                contactCell.areaTextField.isEnabled = false
                contactCell.streetTextField.isEnabled = false
                contactCell.buildingTextField.isEnabled = false
                contactCell.landmarkTextField.isEnabled = false
                contactCell.cityTextField.isEnabled = false
                contactCell.postBoxTextField.isEnabled = false
                contactCell.countryTextField.isEnabled = false
                
                contactCell.driverNameTextField.text = driverInfoDict?.userName
                contactCell.areaCodeTextField.text = driverInfoDict?.countryAreaCode
                contactCell.phoneTextField.text = driverInfoDict?.phone
                contactCell.emailTextField.text = driverInfoDict?.email
                contactCell.areaTextField.text = driverInfoDict?.area
                contactCell.streetTextField.text = driverInfoDict?.street
                contactCell.buildingTextField.text = driverInfoDict?.building
                contactCell.landmarkTextField.text = driverInfoDict?.landmark
                contactCell.cityTextField.text = driverInfoDict?.city
                contactCell.postBoxTextField.text = driverInfoDict?.postBox
                contactCell.countryTextField.text = driverInfoDict?.country
                contactCell.btnCountryCode.titleLabel?.text = driverInfoDict?.countryISO
            }
            else
            {
            contactCell.btnCountryCode.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            }
            return contactCell
        }
        
        if indexPath.row == 2 {
            let emergencyContactCell = collectionView.dequeueReusableCell(withReuseIdentifier: emergencyContactCellIdentifier, for: indexPath) as! EmergencyContactCollectionCell
            
            if(isFromDriverList == true) {
                emergencyContactCell.emgContactPersonTextField.isEnabled = false
                emergencyContactCell.emgAreaCodeTextField.isEnabled = false
                emergencyContactCell.emgPhoneNoTextField.isEnabled = false
                emergencyContactCell.emgEmailTextField.isEnabled = false
                emergencyContactCell.emgAreaTextField.isEnabled = false
                emergencyContactCell.emgStreetTextField.isEnabled = false
                emergencyContactCell.emgBuildingTextField.isEnabled = false
                emergencyContactCell.emgLandmarkTextField.isEnabled = false
                emergencyContactCell.emgCityTextField.isEnabled = false
                emergencyContactCell.emgPoBoxTextField.isEnabled = false
                emergencyContactCell.emgCountryTextField.isEnabled = false
                
                emergencyContactCell.emgContactPersonTextField.text = driverInfoDict?.eContactName
                emergencyContactCell.emgAreaCodeTextField.text = driverInfoDict?.ecountryAreaCode
                emergencyContactCell.emgPhoneNoTextField.text = driverInfoDict?.ePhone
                emergencyContactCell.emgEmailTextField.text = driverInfoDict?.eEmail
                emergencyContactCell.emgAreaTextField.text = driverInfoDict?.eArea
                emergencyContactCell.emgStreetTextField.text = driverInfoDict?.eStreet
                emergencyContactCell.emgBuildingTextField.text = driverInfoDict?.eBuilding
                emergencyContactCell.emgLandmarkTextField.text = driverInfoDict?.eLandmark
                emergencyContactCell.emgCityTextField.text = driverInfoDict?.eCity
                emergencyContactCell.emgPoBoxTextField.text = driverInfoDict?.ePostBox
                emergencyContactCell.emgCountryTextField.text = driverInfoDict?.eCountry
                emergencyContactCell.btnEmgCountryCode.titleLabel?.text = driverInfoDict?.eCountryISO

            }
            else
            {
                emergencyContactCell.btnEmgCountryCode.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            }
            return emergencyContactCell
        }
        
        if indexPath.row == 3 {
            let prevExpCell = collectionView.dequeueReusableCell(withReuseIdentifier: prevWorkExpCellIdentifier, for: indexPath) as! PreviousWorkExpCollectionCell
            if(isFromDriverList == true) {
                
            }
            else
            {
            }
            return prevExpCell
        }
        if indexPath.row == 4 {
            let presentWorkCell = collectionView.dequeueReusableCell(withReuseIdentifier: presentWorkCellIdentifier, for: indexPath) as! presentWorkDetailsCollectionCell
            
            if(isFromDriverList == true) {
                presentWorkCell.nameTextField.text = driverInfoDict?.driverFirstName

               presentWorkCell.userNameTextField.text = driverInfoDict?.userName
              //  presentWorkCell.userTypeTextField.text = driverInfoDict?.userName
               presentWorkCell.empIDtextField.text = driverInfoDict?.employeeId
               presentWorkCell.btnDOJ.titleLabel?.text = driverInfoDict?.presentDateOfJoining
              presentWorkCell.designationTextField.text  = driverInfoDict?.presentdesignation
            
                presentWorkCell.btnShirtSize.titleLabel?.text  = driverInfoDict?.shirtSize
                    presentWorkCell.btnTrouserSize.titleLabel?.text = driverInfoDict?.trouserSize
                        presentWorkCell.btnShoesSize.titleLabel?.text = driverInfoDict?.shoeSize
                presentWorkCell.btnNoOfShirtReceived.titleLabel?.text = driverInfoDict?.noOfShirtsReceived
              presentWorkCell.btnNoOfTrouserReceived.titleLabel?.text = driverInfoDict?.noOfTrousersReceived
               presentWorkCell.btnNoOfShoesReceived.titleLabel?.text = driverInfoDict?.noOfShoeReceived
                presentWorkCell.btnReportingTo.titleLabel?.text = driverInfoDict?.reportingTo?.description
                presentWorkCell.btnWorkingHours.titleLabel?.text = driverInfoDict?.preferedWorkTimings?.description
                presentWorkCell.btnPreferredOffDay.titleLabel?.text = driverInfoDict?.preferedDayOff?.description

            }
            else
            {
            presentWorkCell.btnDOJ.addTarget(self, action: #selector(btnDateOfJoiningClicked(sender:)), for: .touchUpInside)
            
            presentWorkCell.btnWorkingHours.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            
            presentWorkCell.btnPreferredOffDay.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            
            presentWorkCell.btnReportingTo.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            presentWorkCell.btnShirtSize.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            presentWorkCell.btnTrouserSize.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            presentWorkCell.btnShoesSize.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            
            presentWorkCell.btnNoOfShirtReceived.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            
            presentWorkCell.btnNoOfTrouserReceived.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            presentWorkCell.btnNoOfShoesReceived.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            presentWorkCell.btnSelectVehicle.addTarget(self, action: #selector(btnDropDownClicked(sender:)), for: .touchUpInside)
            }
        return presentWorkCell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsMake(0.5,0,0.5,0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat(view.frame.size.width), height: CGFloat(driverCollectionView.frame.size.height))
    }
    
    //MARK: Button Actions
    
    @IBAction func nextBtnClicked(_ sender: UIButton) {
        let selectedRowArray = driverCollectionView.indexPathsForVisibleItems
        var indexPath = selectedRowArray[0]
        if indexPath.row == 0
        {
            let detailCell = driverCollectionView.cellForItem(at: indexPath)as! DriverDetailCollectionCell
            
            if(!detailCell.firstNameTextField.hasText)
            {
                CommonMethod().alertMessage("Error", message:"Please enter First Name", currentVC: self)
                return
            }
            
           else if(!detailCell.lastNameTextField.hasText)
            {
                CommonMethod().alertMessage("Error", message:"Please enter Last Name", currentVC: self)
                return
            }
            else
            {
                driverDetailDict["driverFirstName"] = detailCell.firstNameTextField.text as AnyObject?
                driverDetailDict["driverLastName"] = detailCell.lastNameTextField.text as AnyObject?
                driverDetailDict["licenseNo"] = detailCell.licenseNoTextField.text as AnyObject?
                driverDetailDict["governmentIdNumber"] = detailCell.govtIDTextField.text as AnyObject?
                driverDetailDict["nationality"] = detailCell.nationalityTextField.text as AnyObject?
                driverDetailDict["passportNo"] = detailCell.passportTextField.text as AnyObject?
                driverDetailDict["visaNo"] = detailCell.viseTextField.text as AnyObject?
                driverDetailDict["healthInsuranceCardNo"] = detailCell.insCardNoTextField.text as AnyObject?
                driverDetailDict["licenseNoExpiredAt"] = detailCell.btnLicenceExpiryDate.titleLabel?.text as AnyObject?
                
                driverDetailDict["passportIssuedAt"] = detailCell.btnPassportIssuedDate.titleLabel?.text as AnyObject?
                driverDetailDict["passportExpiredAt"] = detailCell.btnPassportExpiryDate.titleLabel?.text as AnyObject?
                driverDetailDict["visaIssuedAt"] = detailCell.btnVisaIssuedDate.titleLabel?.text as AnyObject?
                driverDetailDict["visaExpiredAt"] = detailCell.btnVisaExpDate.titleLabel?.text as AnyObject?
                driverDetailDict["healthInsuranceCardExpiryAt"] = detailCell.btnInsCardExpDate.titleLabel?.text as AnyObject?
            }
        }
        if indexPath.row == 1
        {
            let contactCell = driverCollectionView.cellForItem(at: indexPath)as! ContactDetailCollectionCell
            if(!contactCell.areaCodeTextField.hasText)
            {
                CommonMethod().alertMessage("Error", message:"Please enter Area Code", currentVC: self)
                return
            }
            
           else if(!contactCell.phoneTextField.hasText)
            {
                CommonMethod().alertMessage("Error", message:"Please enter Phone Number", currentVC: self)
                return
            }
            else
            {
            contactDetailDict["driverName"] = contactCell.driverNameTextField.text as AnyObject?
            contactDetailDict["phone"] = contactCell.phoneTextField.text as AnyObject?
            contactDetailDict["countryCode"] = contactCell.btnCountryCode.titleLabel?.text as AnyObject?
            contactDetailDict["areaCode"] = contactCell.areaCodeTextField.text as AnyObject?
            contactDetailDict["area"] = contactCell.areaTextField.text as AnyObject?
            contactDetailDict["email"] = contactCell.emailTextField.text as AnyObject?
            contactDetailDict["street"] = contactCell.streetTextField.text as AnyObject?
            contactDetailDict["building"] = contactCell.buildingTextField.text as AnyObject?
            contactDetailDict["landmark"] = contactCell.landmarkTextField.text as AnyObject?
            contactDetailDict["city"] = contactCell.cityTextField.text as AnyObject?
            contactDetailDict["postBox"] = contactCell.postBoxTextField.text as AnyObject?
            contactDetailDict["country"] = contactCell.countryTextField.text as AnyObject?
            }
        }
        
        if indexPath.row == 2
        {
            let emergencyContactCell = driverCollectionView.cellForItem(at: indexPath)as! EmergencyContactCollectionCell
            
            emergencyContactDetailDict["emgDriverName"] = emergencyContactCell.emgContactPersonTextField.text as AnyObject?
            emergencyContactDetailDict["emgPhone"] = emergencyContactCell.emgPhoneNoTextField.text as AnyObject?
            emergencyContactDetailDict["emgCountryCode"] = emergencyContactCell.btnEmgCountryCode.titleLabel?.text as AnyObject?
            emergencyContactDetailDict["emgAreaCode"] = emergencyContactCell.emgAreaCodeTextField.text as AnyObject?
            emergencyContactDetailDict["emgEmail"] = emergencyContactCell.emgEmailTextField.text as AnyObject?
            emergencyContactDetailDict["emgArea"] = emergencyContactCell.emgAreaTextField.text as AnyObject?
            emergencyContactDetailDict["emgStreet"] = emergencyContactCell.emgStreetTextField.text as AnyObject?
            emergencyContactDetailDict["emgBuilding"] = emergencyContactCell.emgBuildingTextField.text as AnyObject?
            emergencyContactDetailDict["emgLandmark"] = emergencyContactCell.emgLandmarkTextField.text as AnyObject?
            emergencyContactDetailDict["emgCity"] = emergencyContactCell.emgCityTextField.text as AnyObject?
            emergencyContactDetailDict["emgPostBox"] = emergencyContactCell.emgPoBoxTextField.text as AnyObject?
            emergencyContactDetailDict["emgCountry"] = emergencyContactCell.emgCountryTextField.text as AnyObject?
        }
        
        if indexPath.row == 3
        {
            let prevWorkExpCell = driverCollectionView.cellForItem(at: indexPath)as! PreviousWorkExpCollectionCell
            for i in 0..<prevWorkExpCell.experiencearray.count
            {
                let prevWorkTableCell = prevWorkExpCell.workExpTableView.cellForRow(at: IndexPath(row: i, section: 0))as? ExperienceTableViewCell
                expDict["totalWorkExperience"] = prevWorkTableCell?.totalExpTextField.text as AnyObject?
                expDict["relatedWorkExperience"] = prevWorkTableCell?.relatedExpTextField.text as AnyObject?
                expDict["companyName"] = prevWorkTableCell?.companyNameTextField.text as AnyObject?
                expDict["previousdesignation"] = prevWorkTableCell?.designationTextField.text as AnyObject?
                expDict["dateOfJoining"] = prevWorkTableCell?.btnDateOfJoining.titleLabel?.text as AnyObject?
                expDict["dateOfRelieving"] = prevWorkTableCell?.btnDateOfRelieving.titleLabel?.text as AnyObject?
                expDict["isDeleted"] = false as AnyObject?
                expDict["userId"] = 572 as AnyObject?
                expDict["driverId"] = 0 as AnyObject?
                expDict["isNew"] = true as AnyObject?
                
                prevExpDetailArray.append(expDict)
            }
            
            btnSaveDetail.isHidden = false
            btnCancelDetail.isHidden = false
            btnNext.isHidden = true
        }
        
        if indexPath.row == 4
        {
            
        }
        
        if (indexPath.row < 4)
        {
            indexPath.row += 1
            
            driverCollectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
    @IBAction func previousBtnClicked(_ sender: UIButton) {
        
        prevExpDetailArray .removeAll()
        let selectedRowArray = driverCollectionView.indexPathsForVisibleItems
        var indexPath = selectedRowArray[0]
        btnSaveDetail.isHidden = true
        btnCancelDetail.isHidden = true
        btnNext.isHidden = false
        
        if indexPath.row != 0
        {
            indexPath.row -= 1
            
            driverCollectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
    func btnLicenceExpiryDateClicked(sender : UIButton)  {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        if !self.view.subviews.contains(customCalenderPopUp!)
        {
            self.view.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
        }
    }
    
    func btnPassportIssuedDateClicked(sender : UIButton)  {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        
        if !self.view.subviews.contains(customCalenderPopUp!)
        {
            self.view.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
        }
    }
    
    func btnPassportExpiryDateClicked(sender : UIButton)  {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        
        if !self.view.subviews.contains(customCalenderPopUp!)
        {
            self.view.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
        }
        
    }
    
    func btnVisaIssuedDateClicked(sender : UIButton)  {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        
        if !self.view.subviews.contains(customCalenderPopUp!)
        {
            self.view.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
        }
    }
    
    func btnVisaExpDateClicked(sender : UIButton)  {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        
        if !self.view.subviews.contains(customCalenderPopUp!)
        {
            self.view.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
        }
    }
    
    func btnInsCardExpDateClicked(sender : UIButton)  {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        if !self.view.subviews.contains(customCalenderPopUp!)
        {
            self.view.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
        }
    }
    
    func btnDateOfJoiningClicked(sender : UIButton)  {
        selectedBtn = sender
        customCalenderPopUp = UINib(nibName: "CustomCalenderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomCalendarView
        print(customCalenderPopUp!)
        
        if !self.view.subviews.contains(customCalenderPopUp!)
        {
            self.view.addSubview(customCalenderPopUp!)
            customCalenderPopUp?.delegate = self
        }
    }
    
    func btnDropDownClicked(sender : UIButton)  {
        
        if(sender.tag == 100) //Working Hours
        {
             var tempArray:[String] = []
            
            for object in shiftListArray!{
                let temp = object.shiftName
                tempArray.append(temp!)
                
            }
            self.showDropDown(array: tempArray as NSArray, sender: sender, height: 160)
        }
            
        else if(sender.tag == 101)//Preferred day OFF
        {
            var tempArray:[String] = []
            
            for object in daysArray{
                let temp = object["dayName"]
                tempArray.append(temp! as! String)
            }

     
            self.showDropDown(array: tempArray as NSArray, sender: sender, height: 160)
            
        }
        else if(sender.tag == 102)//Reporting to
        {
            var tempArray:[String] = []
            
            for object in managerListArray!{
                let temp = object.displayName
                tempArray.append(temp!)
                }

            
            self.showDropDown(array: tempArray as NSArray, sender: sender, height: 120)
            
        }
        else if(sender.tag == 103)//Shirt Size
        {
        let arrayOfShirtSize: [String] = ["S", "M","L","XL","XXL","XXXL"]
        
       self.showDropDown(array: arrayOfShirtSize as NSArray, sender: sender, height: 160)
    }
        else if(sender.tag == 104)//Trouser Size
        {
            let arrayOfTrouserSize: [String] = ["28", "30","32","34","36","38","40","42","44","46"]

            self.showDropDown(array: arrayOfTrouserSize as NSArray, sender: sender, height: 160)
            
        }
        else if(sender.tag == 105)//Shoes Size
        {
            let arrayOfShoesSize: [String] = ["06", "07","08","09","10","11"]

            self.showDropDown(array: arrayOfShoesSize as NSArray, sender: sender, height: 160)
            
        }
        else if(sender.tag == 106)//No of Shirt Received
        {
            let arrayOfReceivedShirt: [String] = ["01", "02","03","04","05","06","07","08","09","10"]
            
            self.showDropDown(array: arrayOfReceivedShirt as NSArray, sender: sender, height: 160)
            
        }
        else if(sender.tag == 107)//No of Trouser Received
        {
            let arrayOfReceivedTrouser: [String] = ["01", "02","03","04","05","06","07","08","09","10"]

            
            self.showDropDown(array: arrayOfReceivedTrouser as NSArray, sender: sender, height: 160)
            
        }

        else if(sender.tag == 108)//No of Shoes Received
        {
            let arrayOfReceivedShoes: [String] = ["01", "02","03","04","05","06","07","08","09","10"]
            
            self.showDropDown(array: arrayOfReceivedShoes as NSArray, sender: sender, height: 160)
            
        }

        else if(sender.tag == 109)//Select Vehicle
        {
       
            var tempArray:[String] = []

            for object in truckListArray!{
                let temp = object.truckCode
                tempArray.append(temp!)
            }
            self.showDropDown(array: tempArray as NSArray, sender: sender, height: 120)
        }
        
        else if(sender.tag == 200)//Contact - country code
        {
            
            var tempArray:[String] = []
            
            for object in countryNameArray!{
                let temp = object.CountryCodeDetail
                tempArray.append(temp!)
            }
            self.showDropDown(array: tempArray as NSArray, sender: sender, height: 240)
        }

        
        else if(sender.tag == 210)//Emergency contact -country code
        {
            
            var tempArray:[String] = []
            
            for object in countryNameArray!{
                let temp = object.CountryCodeDetail
                tempArray.append(temp!)
            }
            self.showDropDown(array: tempArray as NSArray, sender: sender, height: 240)
        }

        
}
    
    //MARK: Custom Calenar View Delegate
    func btnOkClicked(_ sender: UIButton, datestring:String)
    {
        selectedBtn.setTitle(datestring, for: .normal)
        if self.view.subviews.contains(customCalenderPopUp!) {
            customCalenderPopUp?.removeFromSuperview()
        }
    }
    
    func btnCancelClicked(_ sender: UIButton)
    {
        if self.view.subviews.contains(customCalenderPopUp!) {
            customCalenderPopUp?.removeFromSuperview()
        }
    }
    
    //MARK: Dismiss KeyBoard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func btnSaveDetailClicked(_ sender: UIButton) {
        let selectedRowArray = driverCollectionView.indexPathsForVisibleItems
        let indexPath = selectedRowArray[0]
        
        let presentWorkDetailCell = driverCollectionView.cellForItem(at: indexPath)as! presentWorkDetailsCollectionCell
        
        if(!presentWorkDetailCell.userNameTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter Username", currentVC: self)
            return
        }
            
        else if(!presentWorkDetailCell.designationTextField.hasText)
        {
            CommonMethod().alertMessage("Error", message:"Please enter Desigation", currentVC: self)
            return
        }
        else if(selectedManagerID == -1)
        {
            CommonMethod().alertMessage("Error", message:"Please select reporting to", currentVC: self)
            return
        }
            
        else
        {
       MBProgressHUD.showAdded(to: self.view, animated:
            true)
        self.setDriverDetailInDictionary()
        
        WebserviceHelper() .callRegisterDriverAPI(parameters: regDriverParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if let data = response.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    let serviveMsg = (json as! NSDictionary)["serviceMessage"] as! String
                    CommonMethod().alertMessage("Status", message:serviveMsg as NSString , currentVC: self)
                } catch {
                    CommonMethod().alertMessage("Status", message:"Parse error" as NSString , currentVC: self)
                }
            }
        }
        }
    }
    
    @IBAction func btnCancelDetailClicked(_ sender: UIButton) {
        
    }
    
    func setDriverDetailInDictionary(){
        
        let selectedRowArray = driverCollectionView.indexPathsForVisibleItems
        let indexPath = selectedRowArray[0]
        
        let presentWorkDetailCell = driverCollectionView.cellForItem(at: indexPath)as! presentWorkDetailsCollectionCell
    
        //Personal Details
        regDriverParams["licenseNoExpiredAt"] = driverDetailDict["licenseNoExpiredAt"] as AnyObject?
        regDriverParams["passportIssuedAt"] = driverDetailDict["passportIssuedAt"] as AnyObject?
        regDriverParams["visaIssuedAt"] = driverDetailDict["visaIssuedAt"] as AnyObject?
        regDriverParams["passportExpiredAt"] = driverDetailDict["passportExpiredAt"] as AnyObject?
        regDriverParams["visaExpiredAt"] = driverDetailDict["visaExpiredAt"] as AnyObject?
        regDriverParams["healthInsuranceCardExpiryAt"] = driverDetailDict["healthInsuranceCardExpiryAt"]as AnyObject?
        regDriverParams["governmentIdNumber"] = driverDetailDict["governmentIdNumber"] as AnyObject?
        regDriverParams["nationality"] = driverDetailDict["nationality"] as AnyObject?
        regDriverParams["clientId"] = "1" as AnyObject
        regDriverParams["driverId"] = 0 as AnyObject
        regDriverParams["driverFirstName"] = driverDetailDict["driverFirstName"] as AnyObject?
        regDriverParams["driverLastName"] = driverDetailDict["driverLastName"] as AnyObject?
        regDriverParams["licenseNo"] = driverDetailDict["licenseNo"] as AnyObject?
        regDriverParams["passportNo"] = driverDetailDict["passportNo"] as AnyObject?
        regDriverParams["visaNo"] = driverDetailDict["visaNo"] as AnyObject?
        regDriverParams["healthInsuranceCardNo"] = driverDetailDict["healthInsuranceCardNo"] as AnyObject?
        regDriverParams["userId"] = 572 as AnyObject
        
        //Contact Details
        regDriverParams["driverName"] = contactDetailDict["driverName"] as AnyObject?
        regDriverParams["countryCode"] = "971" as AnyObject
        //contactDetailDict["countryCode"]
        regDriverParams["areaCode"] = contactDetailDict["areaCode"] as AnyObject?
        regDriverParams["phone"] = contactDetailDict["phone"] as AnyObject?
        regDriverParams["building"] = contactDetailDict["building"] as AnyObject?
        regDriverParams["email"] = contactDetailDict["email"] as AnyObject?
        regDriverParams["street"] = contactDetailDict["street"] as AnyObject?
        regDriverParams["landmark"] = contactDetailDict["landmark"] as AnyObject?
        regDriverParams["city"] = contactDetailDict["city"] as AnyObject?
        
        //Emergency Contacts Details
        regDriverParams["eContactName"] = emergencyContactDetailDict["emgDriverName"]
        regDriverParams["eCountryCode"] = "94" as AnyObject
        //emergencyContactDetailDict["emgCountryCode"]
        regDriverParams["eAreaCode"] = emergencyContactDetailDict["emgAreaCode"]
        regDriverParams["ephone"] = emergencyContactDetailDict["emgPhone"]
        regDriverParams["earea"] = emergencyContactDetailDict["emgArea"]
        regDriverParams["ebuilding"] = emergencyContactDetailDict["emgBuilding"]
        regDriverParams["eemail"] = emergencyContactDetailDict["emgAreaCode"]
        regDriverParams["estreet"] = emergencyContactDetailDict["emgStreet"]
        regDriverParams["elandmark"] = emergencyContactDetailDict["emgLandmark"]
        regDriverParams["epostBox"] = emergencyContactDetailDict["emgPostBox"]
        regDriverParams["ecity"] = emergencyContactDetailDict["emgCity"]
        
        //Driver Previous Experience
    regDriverParams["driverDetailsList"] = prevExpDetailArray as AnyObject?
        
        //Driver Present Experience Details
        
        regDriverParams["userName"] = presentWorkDetailCell.userNameTextField.text as AnyObject?
        regDriverParams["employeeId"] = presentWorkDetailCell.empIDtextField.text as AnyObject?
        regDriverParams["dateOfJoining"] = presentWorkDetailCell.btnDOJ.titleLabel?.text as AnyObject?
        regDriverParams["presentdesignation"] = presentWorkDetailCell.designationTextField.text as AnyObject?
        
        let shiftObj = shiftListArray?[selectedShiftID]
        regDriverParams["preferedWorkTimings"] = shiftObj?.shiftDayId as AnyObject?
       
        let dayObj = daysArray[selectedDayOffID]
        regDriverParams["preferedDayOff"] = dayObj["dayId"] as AnyObject?
        
        let reportObj = managerListArray?[selectedManagerID]
        regDriverParams["reportingTo"] = reportObj?.userID as AnyObject?
        
       
    regDriverParams["shirtSize"] = presentWorkDetailCell.btnShirtSize.titleLabel?.text as AnyObject?
        regDriverParams["trouserSize"] = presentWorkDetailCell.btnTrouserSize.titleLabel?.text as AnyObject?
        regDriverParams["shoeSize"] = presentWorkDetailCell.btnShoesSize.titleLabel?.text as AnyObject?
        regDriverParams["noOfShirtsReceived"] = presentWorkDetailCell.btnNoOfShirtReceived.titleLabel?.text as AnyObject?
        regDriverParams["noOfTrousersReceived"] = presentWorkDetailCell.btnNoOfTrouserReceived.titleLabel?.text as AnyObject?
        regDriverParams["noOfShoeReceived"] = presentWorkDetailCell.btnNoOfShoesReceived.titleLabel?.text as AnyObject?

        if selectedVehicleID == -1 {
            regDriverParams["preferedTruckId"] = nil
        }
        else
        {
            let truckObj = truckListArray?[selectedVehicleID]
            regDriverParams["preferedTruckId"] = truckObj?.truckId as AnyObject?
        }
               
        regDriverParams["locationLatitude"] = "25.2048" as AnyObject?
        regDriverParams["locationLongitude"] = "55.2708" as AnyObject?
        regDriverParams["timeZone"] = "Asia/Dubai" as AnyObject?
        regDriverParams["formattedDateOfJoining"] = presentWorkDetailCell.btnDOJ.titleLabel?.text as AnyObject?
    }
    
    //MARK: ShowDropDown
    
    func showDropDown(array :NSArray ,sender :UIButton, height :CGFloat)  {
        
        if(dropDown == nil)
        {
            var f : CGFloat
            f =  height
            
            dropDown = NIDropDown()
            dropDown?.show(sender,&f, array as! [Any], nil, "down")
            
            // self.view.superview?.bringSubview(toFront: dropDown!)
            
            dropDown?.delegate = self
            
            
        }else
        {
            let stringOldTag =  UserDefaults.standard.value(forKey: "tableTag") as! NSNumber
            let view = self.view.viewWithTag(stringOldTag.intValue) as! UIButton
            dropDown?.hide(view)
            self.releaseDropDown()
        }
        
        let stringNewTag = sender.tag
        UserDefaults.standard.set(stringNewTag, forKey: "tableTag" )
    }
    
    func NIDropDownDismissed(indexPath :NSInteger ,sender :NSInteger) {
        
    }

    
   

    
}

//MARK: NidropdownDelegates

extension DriversRegistrationViewController : NIDropDownDelegate{
    
    func niDropDownDelegateMethod(_ sender: NIDropDown!) {
        self .releaseDropDown()
    }
    func releaseDropDown(){
        dropDown = nil
    }
    
    func niDropDownDismissed(_ indexPath: Int, sender tag: Int, sender btnSender: UIButton!) {

        if(tag == 100)
        {
        selectedShiftID = indexPath
        }
        else if(tag == 101)
        {
            selectedDayOffID = indexPath
        }

        else if(tag == 102)
        {
            selectedManagerID = indexPath
        }
        else if(tag == 103)
        {
            selectedShirtSizeID = indexPath
        }
      else if(tag == 104)
        {
            selectedTrouserSizeID = indexPath
        }
        else if(tag == 105)
        {
            selectedShoesSizeID = indexPath
        }
        else if(tag == 106)
        {
            selectedNoOfShirtID = indexPath
        }
        else if(tag == 107)
        {
            selectedNoOfTrouserID = indexPath
        }
        else if(tag == 108)
        {
            selectedNoOfShoesID = indexPath
        }
        else if(tag == 109)
        {
            selectedVehicleID = indexPath
        }
        
    }
    
    func hideDropDown(_ b: UIButton!) {
        dropDown?.hide(b)
    }
    
}
