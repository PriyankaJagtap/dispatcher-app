//
//  EmergencyContactCollectionCell.swift
//  DispatcherApp
//
//  Created by Webwerks on 27/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class EmergencyContactCollectionCell: UICollectionViewCell,UITextFieldDelegate {

    @IBOutlet weak var emgContactPersonTextField: UITextField!
   
    @IBOutlet weak var btnEmgCountryCode: UIButton!
   @IBOutlet weak var emgAreaCodeTextField: UITextField!
    @IBOutlet weak var emgPhoneNoTextField: UITextField!
    @IBOutlet weak var emgEmailTextField: UITextField!
    @IBOutlet weak var emgAreaTextField: UITextField!
    
    @IBOutlet weak var emgStreetTextField: UITextField!
    
    @IBOutlet weak var emgBuildingTextField: UITextField!
    
    @IBOutlet weak var emgLandmarkTextField: UITextField!
   @IBOutlet weak var emgCityTextField: UITextField!
    
    @IBOutlet weak var emgPoBoxTextField: UITextField!
    
    @IBOutlet weak var emgCountryTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
