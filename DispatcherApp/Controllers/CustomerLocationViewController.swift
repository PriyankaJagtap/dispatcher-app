//
//  CustomerLocationViewController.swift
//  Carpal_Varsha
//
//  Created by Webwerks on 30/05/17.
//  Copyright © 2017 Webwerks. All rights reserved.
//

import UIKit
import GoogleMaps


class CustomerLocationViewController: UIViewController ,UITextFieldDelegate,CLLocationManagerDelegate{

    var myview = MapView()
    var locationManager = CLLocationManager()

    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet weak var addressTitleTextField: UITextField!
    
    @IBOutlet weak var areaTextField: UITextField!
    
    
    @IBOutlet weak var streetTextField: UITextField!
    
    @IBOutlet weak var buildingTextField: UITextField!
    
    @IBOutlet weak var landmarkTextField: UITextField!
    
    
    @IBOutlet weak var countryTextField: UITextField!
    
    @IBOutlet weak var poBoxTextField: UITextField!
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var saveAddressBtn: UIButton!
    
    @IBOutlet weak var mapConatinerViw: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Map Setup
        myview  = MapView.instanceFromNib()
        myview.frame = CGRect(x: 0, y: 0, width: mapConatinerViw.frame.size.width, height: mapConatinerViw.frame.size.height)
        
        myview.layoutSubviews()
        self.mapConatinerViw.addSubview(myview)
        myview.mapView?.isMyLocationEnabled = true
        locationManager.delegate = self
        locationManager.startUpdatingLocation()

        
        let dict = SharedInstanceClass.sharedInstance.caseData?.caseDetailsDictionary
        addressTitleTextField.text = dict?.addressTitle
        areaTextField.text = dict?.area
        streetTextField.text = dict?.street
        buildingTextField.text = dict?.building
        landmarkTextField.text = dict?.landmark
        countryTextField.text = dict?.country
        poBoxTextField.text = dict?.poBox
        cityTextField.text = dict?.city

    }
    //MARK: Textfiled Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func saveAddressBtnClicked(_ sender: Any) {
    }

    @IBAction func editBtnClicked(_ sender: Any) {
    }
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        myview.mapView.animate(to: camera)
        
        let marker = GMSMarker()
//        let imageMarker = UIImage (named: "Customer_Location_Pin")
//        let markerView = UIImageView(image: imageMarker)
//        marker.iconView = markerView
        
        marker.position = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
        marker.title = "Current Location"
        marker.map = myview.mapView
        
        self.locationManager.stopUpdatingLocation()
        
    }

}
