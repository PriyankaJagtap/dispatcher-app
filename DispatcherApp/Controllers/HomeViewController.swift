//
//  HomeViewController.swift
//  DispatcherApp
//
//  Created by varsha on 5/22/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD
import GoogleMaps
import SwipeCellKit
//import SlideMenuControllerSwift


class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,SwipeTableViewCellDelegate {

    @IBOutlet weak var customNavView: UIView!
    var caseListArrayData : [CaseListData]?
    var locationManager = CLLocationManager()
    
    var myview = MapView()
    @IBOutlet weak var caseListTableView: UITableView!
   
 
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapContainerView: UIView!
    
    @IBAction func leftMenuBtnClicked(_ sender: Any) {
        slideMenuController()?.toggleLeft()
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getCaseList(searchText: "")
        caseListArrayData = [CaseListData]()
        searchBar.delegate = self
//        searchBar.showsCancelButton = true
        self.caseListTableView.decelerationRate = UIScrollViewDecelerationRateNormal
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
        self.navigationController?.navigationBar.isHidden = true
      

    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBAction func mapBtnClick(_ sender: UIButton) {
        self.view .endEditing(true)
        if mapContainerView.isHidden{
            mapContainerView.isHidden = false
            if UIImage(named: "list") != nil {
                mapBtn.setImage(UIImage(named: "list"), for: .normal)
            }
            var isMapPresent = false
            let subviews =  mapContainerView.subviews
            for view in subviews{
                if view.isKind(of: MapView.self){
                    isMapPresent = true
                    break
                }
            }
            if !isMapPresent{
                 myview  = MapView.instanceFromNib()
            mapContainerView.isHidden = false
            myview.frame = CGRect(x: 0, y: 0, width: mapContainerView.frame.size.width, height: mapContainerView.frame.size.height)
                
            myview.layoutSubviews()
            self.mapContainerView.addSubview(myview)
                myview.mapView?.isMyLocationEnabled = true
                
                //Location Manager code to fetch current location
                locationManager.delegate = self
                locationManager.startUpdatingLocation()
                
            }
        }else{
            if UIImage(named: "map_view") != nil {
                mapBtn.setImage(UIImage(named: "map_view"), for: .normal)
            }
            
             mapContainerView.isHidden = true
            
        }
    }

    
    func getCaseList(searchText : String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let loginParams: [String: Any] = ["typeId" : -1 as AnyObject, "userId" : "864" as AnyObject, "clientId" : "1" as AnyObject, "startIndex" : 0 as AnyObject ,"endIndex" : 50 as AnyObject, "sortFieldName" : "" as AnyObject, "ascending" : false,"searchParameter" : "caseNumber","valueToBeSearched" : searchText] as Any as! [String : Any]
        
        WebserviceHelper() .callGetCaseListApi(parameters: loginParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responceMsg = CaseList(JSONString : response)
            
            if responceMsg?.isSuccess == true{
                print("success")
                self.caseListArrayData = responceMsg?.caseListArray
                DispatchQueue.main.async {
                    self.caseListTableView.reloadData()
                }
            }
            else
            {
                CommonMethod().alertMessage("Status", message:"Please try again" as NSString , currentVC: self)
                print("error")
                
            }
            
        }

    }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (caseListArrayData?.count)!

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
     
        let cell:CaseListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CaseListTableCell") as! CaseListTableViewCell!
        let caseData : CaseListData = (self.caseListArrayData?[indexPath.row])!
        cell.lblCustomerName.text = caseData.customerName
        cell.lblPhoneNumber.text = caseData.mobileNumber
        cell.lblCaseNo.text = "Case: " +  (caseData.caseID?.description)!
        cell.delegate = self
        cell.lblStatus.text = caseData.status
        if(cell.lblStatus.text == "Case confirmed")
        {
            cell.lblStatus.textColor = UIColor.green
        }

        cell.lblLocation.text = caseData.location
        cell.lblVehicle.text = caseData.vehicle

        
        return cell
    }
    
    func tableView (_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
        self.navigationController?.pushViewController(loginPageView, animated: true)
        let caseData : CaseListData = (self.caseListArrayData?[indexPath.row])!

        SharedInstanceClass.sharedInstance.selectedCaseID = caseData.caseID
//        let nav = UINavigationController(rootViewController: loginPageView)
//        self.present(nav, animated: true, completion: nil)
    }
        
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            // handle action by updating model with deletion
        }
        
    //    deleteAction.image = UIImage(named: "sidemenu")
        
     
        let flagAction = SwipeAction(style: .destructive, title: "Flag") { action, indexPath in
            // handle action by updating model with deletion
        }
        
     //   flagAction.image = UIImage(named: "Flag-circle")
        let moreAction = SwipeAction(style: .destructive, title: "More") { action, indexPath in
            // handle action by updating model with deletion
        }
        
        //    moreAction.image = UIImage(named: "More-circle")

        return [deleteAction,moreAction,flagAction]
    }
    
   /* func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
//        searchActive = false;
        self.searchBar.endEditing(true)
        self.view .endEditing(true)
    }
   */
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        myview.mapView.animate(to: camera)
        
                        let marker = GMSMarker()
//                        let imageMarker = UIImage(named: "Customer_Location_Pin")!.withRenderingMode(.alwaysTemplate)
                        let imageMarker = UIImage (named: "Customer_Location_Pin")
                        let markerView = UIImageView(image: imageMarker)
                        marker.iconView = markerView

                        marker.position = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
                        marker.title = "Current Location"
                        marker.map = myview.mapView
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }
}

extension HomeViewController:UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty  {
            //callSearchServiceWith(param:searchText, size: PageManager.pageSize, page: "1")
            
            
            
        }
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        //showSearchBar()
        view.endEditing(true)
    }
    func hideKeyboard(with searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        //let commentText = searchBar.text
        
        //if((commentText?.characters.count)! > 0)
        
            self.getCaseList(searchText: searchBar.text!)
        
        self.searchBar.text = ""
        self.searchBar.endEditing(true)
        view.endEditing(false)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //        searchActive = false;
        
        self.getCaseList(searchText: searchBar.text!)
        self.searchBar.endEditing(true)
        self.view .endEditing(true)
    }
}

/*extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}*/

