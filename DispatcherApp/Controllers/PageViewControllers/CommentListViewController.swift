//
//  CommentListViewController.swift
//  DispatcherApp
//
//  Created by varsha on 5/31/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD

class CommentListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var dropDown : NIDropDown?
    @IBOutlet weak var customerDropDownBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var commentTextField: UITextField!
    var commentListArray : NSMutableArray=[]
   
    @IBOutlet weak var commentTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customerDropDownBtn?.isHidden = true
        
        commentListArray = SharedInstanceClass.sharedInstance.caseData?.caseCommentListArray as! NSMutableArray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dropDown?.removeFromSuperview()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentListArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:CommentListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CommentListTableViewCell") as! CommentListTableViewCell!
        
        let commentListData : CaseCommentListData = (self.commentListArray[indexPath.row] as! CaseCommentListData)
        
        cell.loggerNameLbl.text = commentListData.loggerName
        cell.descriptionLbl.text = commentListData.description
        cell.commentLbl.text = commentListData.comment
        
        
        return cell
    }


    @IBAction func shareBtnClikced(_ sender: Any) {
        if (shareBtn.isSelected == true)
        {
        self.shareBtn.isSelected = false
        self.customerDropDownBtn?.isHidden = true
        }
        else
        {
         self.customerDropDownBtn?.isHidden = false
        self.shareBtn.isSelected = true

        }
    }
    
    
    @IBAction func dropDownClicked(_ sender: UIButton) {
        var someStrs = [String]()
        someStrs.append("varsha gaikwad")
        someStrs.append("ShibliePortal")
        
        self.showDropDown(array: someStrs as NSArray, sender: sender)
  
    }
    @IBAction func addCommentBtnClicked(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let commentText = self.commentTextField.text
        
        let commentParams: [String: AnyObject] = ["userId":"864" as AnyObject, "caseId":"5596" as AnyObject, "driverId":"185" as AnyObject, "shareWithDriver" : "1" as AnyObject,"assistanceId":"5624"as AnyObject,"caseComments":commentText as AnyObject]
        
        WebserviceHelper() .callCommentAPI(parameters: commentParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responceMsg = CaseDetails(JSONString : response)
            if responceMsg?.isSuccess == true
            {
            self.commentListArray = responceMsg?.caseCommentListArray as! NSMutableArray
             self.commentTableView.reloadData()
            }
            else
            {
               let errorMsg = responceMsg?.serviceMessage
                CommonMethod().alertMessage("Status", message:errorMsg! as NSString , currentVC: self)
                print("error")
                
            }
            
        }

    }
    
    //MARK: ShowDropDown
    
    func showDropDown(array :NSArray ,sender :UIButton)  {
        
        if(dropDown == nil)
        {
            var f : CGFloat
            
            f =  80
            dropDown = NIDropDown()
            dropDown?.show(sender,&f, array as! [Any], nil, "down")
                        
            //self.view.superview?.bringSubview(toFront: dropDown!)
            
            dropDown?.delegate = self
    
        }
//            else
//        {
//            let view = self.view.viewWithTag(sender.tag) as! UIButton
//            dropDown?.hide(view)
//           self.releaseDropDown()
//       }
        }
    
    //func NIDropDownDismissed(indexPath :NSInteger ,sender :NSInteger) {
        
    //}
}

//MARK: NidropdownDelegates

extension CommentListViewController : NIDropDownDelegate{
    
    func niDropDownDelegateMethod(_ sender: NIDropDown!) {
        self .releaseDropDown()
    }
    func releaseDropDown(){
        dropDown = nil
    }
    
    func niDropDownDismissed(_ indexPath: Int, sender tag: Int, sender btnSender: UIButton!) {
        // selectedCountryCode = (btnSender.titleLabel?.text)!
    }
    
    func hideDropDown(_ b: UIButton!) {
        dropDown?.hide(b)
    }

}

