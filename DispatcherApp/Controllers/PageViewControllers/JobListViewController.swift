//
//  JobListViewController.swift
//  DispatcherApp
//
//  Created by varsha on 5/31/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class JobListViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    var caseJobListArrayData: NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        caseJobListArrayData = SharedInstanceClass.sharedInstance.caseData?.caseDetailJobListArray as! NSMutableArray

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (caseJobListArrayData.count)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:JobListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "JobListTableViewCell") as! JobListTableViewCell!
        
        let caseJoblistData : CaseDetailsJobListData = (self.caseJobListArrayData[indexPath.row] as! CaseDetailsJobListData)

//        cell.jobID.text = (caseJoblistData.jobID)?.description
        cell.serviceLbl.text = caseJoblistData.jobService
        cell.statusLbl.text = caseJoblistData.jobStatus ?? ""
        cell.descriptionLbl.text = caseJoblistData.jobFaultDescription
        cell.driverNameLbl.text = caseJoblistData.jobDriverName
        cell.createdDateLbl.text = caseJoblistData.jobCreatedDate

        
//        let cell:CaseListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CaseListTableCell") as! CaseListTableViewCell!
//        let caseData : CaseDetailsJobListData = (self.caseJobListArrayData[indexPath.row] as! CaseDetailsJobListData)
////        cell.lblCaseNo.text =   caseData.jobService
//        cell.lblStatus.text = caseData.jobStatus
//       
//        cell.lblLocation.text = caseData.jobFaultDescription
//        cell.lblVehicle.text = caseData.jobCreatedDate
//        cell.lblCustomerName.text = caseData.jobDriverName

        
        return cell
    }

   
    @IBAction func addJobBtnClick(_ sender: UIButton) {
        let viewController:UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "AddJobViewController") as! AddJobViewController
        self.present(viewController, animated: false, completion: nil)
    }

}
