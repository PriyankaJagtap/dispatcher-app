//
//  JobListTableViewCell.swift
//  DispatcherApp
//
//  Created by varsha on 5/31/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class JobListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var jobID: UILabel!
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var createdDateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
