//
//  CommentListTableViewCell.swift
//  DispatcherApp
//
//  Created by varsha on 5/31/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class CommentListTableViewCell: UITableViewCell {

    @IBOutlet weak var loggerNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
