//
//  CaseClosingViewController.swift
//  DispatcherApp
//
//  Created by Webwerks on 06/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD

class CaseClosingViewController: UIViewController,UITextFieldDelegate {
   
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var customerPhoneLabel: UILabel!
    @IBOutlet weak var suggestionTextField: UITextField!
    
       override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func submitBtnClicked(_ sender: Any) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let suggestionText = self.suggestionTextField.text
        
        let caseCloseParams: [String: AnyObject] = ["userId":"864" as AnyObject, "caseId":"5596" as AnyObject, "customerCareResponse":"4" as AnyObject, "driverResponse" : "4" as AnyObject,"servicesProvided":"4"as AnyObject,"overallRating":"5" as AnyObject,"suggestions":suggestionText as AnyObject]
        
        WebserviceHelper() .callCaseClosingAPI(parameters: caseCloseParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
           
            if let data = response.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                    
                    let errorMsg = json?["serviceMessage"]
                    CommonMethod().alertMessage("Status", message:errorMsg! as! NSString , currentVC: self)
                    
                } catch {
                }
            }
         
        }

    }
    
}
