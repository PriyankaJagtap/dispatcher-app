//
//  AdditionDetailsViewController.swift
//  TableDemoObjectivec
//
//  Created by Sonali on 5/30/17.
//  Copyright © 2017 com.neosofttech. All rights reserved.
//

import UIKit

class AdditionDetailsViewController: UIViewController,FSCalendarDelegate,FSCalendarDataSource{
    var dropDown : NIDropDown?
  
    
    @IBOutlet weak var calendarView: FSCalendar!
    
    //MARK: IBOutlet
   // private weak var calendar: FSCalendar!

    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var viewSaveCancelBtn: UIView!
    @IBOutlet weak var viewScheduled: UIView!
    @IBOutlet weak var btnScheduled: UIButton!
    @IBOutlet weak var btnImmediately: UIButton!
    
    @IBOutlet weak var dateButton: UIButton!
    //MARK: Constraint

    @IBOutlet weak var heightScheduledConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintEditBtnView: NSLayoutConstraint!
      @IBOutlet weak var heightImmediatViewConstraint: NSLayoutConstraint!
    
   
    @IBAction func dateBtnClicked(_ sender: UIButton) {
        if (dateButton.isSelected == true)
        {
            self.dateButton.isSelected = false
            calendarView.isHidden = true

        }
        else
        {
            self.dateButton.isSelected = true
            calendarView.isHidden = false
         }
    }
    
   //MARK: Cancel CLicked
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        viewScheduled.isHidden = true
        btnEdit.isHidden = false
        viewSaveCancelBtn.isHidden = true
        btnImmediately.isSelected = true
    }
   
    //MARK: ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnImmediately.isSelected = true
        viewScheduled.isHidden = true
        viewSaveCancelBtn.isHidden = true
      
        calendarView.isHidden = true
    }
    
    //MARK: Radio Clicked
    
    @IBAction func btnRadioClicked(_ sender: UIButton) {
        if(sender.tag == 10){
            self.clickImmediateRadioBtn() }
        else{
            self.clickScheduledRadioBtn() }
        }
    
    //click immediate btn
    func clickImmediateRadioBtn() {
        self.btnImmediately.isSelected = true
        self.btnScheduled.isSelected = false
        viewScheduled.isHidden = true;
        heightScheduledConstraint.constant = 0;
        heightConstraintEditBtnView.constant = 0;
        btnEdit.isHidden = true
    }
    //click schedule btn
    func clickScheduledRadioBtn() {
        btnEdit.isHidden = true
        viewScheduled.isHidden = false;
        self.btnImmediately.isSelected = false
        self.btnScheduled.isSelected = true
        heightConstraintEditBtnView.constant = 0
        heightScheduledConstraint.constant = 127
        viewSaveCancelBtn.isHidden = false
    }
    
     //MARK: Edit Clicked
    @IBAction func btnEditClicked(_ sender: UIButton) {

    }
    
    //MARK: DropDown Clicked

    @IBAction func btnDropDownClicked(_ sender: UIButton) {
        
        if(sender.tag == 110) //client btn
        {
            var someStrs = [String]()
            someStrs.append("MENA Assistance")
            someStrs.append("ShibliePortal")
            
            self.showDropDown(array: someStrs as NSArray, sender: sender, height: 80)
            
        }else if(sender.tag == 111)//case Priority
        {
            
            var someStrs = [String]()
            
            someStrs.append("High")
            someStrs.append("Medium")
            someStrs.append("Low")
            
            self.showDropDown(array: someStrs as NSArray, sender: sender, height: 120)
        }else if(sender.tag == 220)//hours
        {
       var hourArray = [AnyObject]()
            for i in 0..<24 {
               hourArray.append(i as AnyObject)
            }

            self.showDropDown(array: hourArray as NSArray, sender: sender, height: 120)
        }
        else if(sender.tag == 210)//minute
        {
            var minuteArray = [AnyObject]()
            for i in 0..<60 {
                minuteArray.append(i as AnyObject)
            }
     
            self.showDropDown(array: minuteArray as NSArray, sender: sender, height: 120)
        }

    }
    
    //MARK: ShowDropDown

    func showDropDown(array :NSArray ,sender :UIButton, height :CGFloat)  {
        
        if(dropDown == nil)
        {
            var f : CGFloat
            f =  height
            
            dropDown = NIDropDown()
            dropDown?.show(sender,&f, array as! [Any], nil, "down")
            
           // self.view.superview?.bringSubview(toFront: dropDown!)

            dropDown?.delegate = self
         
            
        }else
        {
            let stringOldTag =  UserDefaults.standard.value(forKey: "tableTag") as! NSNumber
            let view = self.view.viewWithTag(stringOldTag.intValue) as! UIButton
            dropDown?.hide(view)
            self.releaseDropDown()
        }
        
        let stringNewTag = sender.tag
        UserDefaults.standard.set(stringNewTag, forKey: "tableTag" )
    }
    
    func NIDropDownDismissed(indexPath :NSInteger ,sender :NSInteger) {
        
    }

    //MARK: FSCalender Delegate
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
            
            }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString = dateFormatter.string(from: date)
        
        self.dateButton.setTitle(dateString, for: .normal)
        
    }
}


//MARK: NidropdownDelegates

extension AdditionDetailsViewController : NIDropDownDelegate{
    
    func niDropDownDelegateMethod(_ sender: NIDropDown!) {
        self .releaseDropDown()
    }
    func releaseDropDown(){
        dropDown = nil
    }
    
    func niDropDownDismissed(_ indexPath: Int, sender tag: Int, sender btnSender: UIButton!) {
        // selectedCountryCode = (btnSender.titleLabel?.text)!
    }
    
    func hideDropDown(_ b: UIButton!) {
        dropDown?.hide(b)
    }

}
