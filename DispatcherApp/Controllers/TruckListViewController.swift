//
//  TruckListViewController.swift
//  DispatcherApp
//
//  Created by Webwerks on 15/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD

class TruckListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var truckListArr : [truckListObj]?
    @IBOutlet weak var truckListTableView: UITableView!
    
    @IBAction func sideMenuBtnClicked(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()
}
    override func viewDidLoad() {
        super.viewDidLoad()
          }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
        truckListArr = [truckListObj]()
        self .callTruckListWS()
        }
    
    func callTruckListWS() {
        MBProgressHUD.showAdded(to: self.view, animated: true)

        let truckListParams: [String: AnyObject] = ["clientId":"8" as AnyObject, "timeZone":"Asia/Dubai" as AnyObject]
        
        WebserviceHelper() .calltruckListAPI(parameters: truckListParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responce = TruckList(JSONString : response)
            if responce?.isSuccess == true
            {
                
                if let data = responce?.truckList{
                    self.truckListArr = data
                    DispatchQueue.main.async {
                        self.truckListTableView.reloadData()
                    }
                }
            }
            else
            {
             CommonMethod().alertMessage("Status", message:"Server error", currentVC: self)
            }
            }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (truckListArr?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TruckListCustomCell = tableView.dequeueReusableCell(withIdentifier: "TruckListCustomCell") as! TruckListCustomCell!
        
        if let truck : truckListObj = self.truckListArr?[indexPath.row]
        {
       cell.truckCodeLabel.text = truck.truckCode
            
           
           
             cell.issuesDateLbl.text = truck.truckPolicyIssuedDate
             cell.expDateLbl.text = truck.truckPolicyExpDate
             cell.caseAttendedDate.text = truck.lastcaseAttendedTime
             cell.regDueDateLbl.text = truck.truckReRegDueDate
        }
        
        cell.deactivateBtn?.addTarget(self, action: #selector(deactivateBtnClicked), for: .touchUpInside)
        
        cell.removeBtn?.addTarget(self, action: #selector(removeBtnClicked), for: .touchUpInside)
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let truck : truckListObj = self.truckListArr?[indexPath.row]
        {
            
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let truckRegVC = storyboard.instantiateViewController(withIdentifier: "TruckRegistrationViewController") as! TruckRegistrationViewController
            truckRegVC.selectedTruck = truck
            truckRegVC.isFromTruckList = true
            navigationController?.pushViewController(truckRegVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func deactivateBtnClicked(sender:UIButton ) {
        
    }
    
    func removeBtnClicked(sender:UIButton ) {
        
    }
    

}
