//
//  AddJobViewController.swift
//  DispatcherApp
//
//  Created by varsha on 6/4/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class AddJobViewController: UIViewController ,JKExpandTableViewDelegate, JKExpandTableViewDataSource{
    var faultServiceArray : NSMutableArray=[]
    var faultServiceSubListArray : NSMutableArray=[]

    @IBOutlet weak var expandTableView: JKExpandTableView!

    @IBAction func doneBtnClick(_ sender: UIButton) {
        self .dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        faultServiceArray = SharedInstanceClass.sharedInstance.caseData?.caseFaultServicesArray as! NSMutableArray
        self.expandTableView.tableViewDelegate = self
        self.expandTableView.dataSourceDelegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfParentCells() -> Int {
        return  faultServiceArray.count
    }
    func number(ofChildCellsUnderParentIndex parentIndex: Int) -> Int {
//        return  2
        return (SharedInstanceClass.sharedInstance.caseData?.caseFaultServicesArray?[parentIndex].caseFaultServiceSubListArray?.count)!
        
    }
    func labelForParentCell(at parentIndex: Int) -> String! {
        let caseData : CaseFaultSErvicesListData = (self.faultServiceArray[parentIndex] as! CaseFaultSErvicesListData)

        return caseData.caseFaultServiceName
    }
    func labelForCell(atChildIndex childIndex: Int, withinParentCellIndex parentIndex: Int) -> String! {
       // return "hello"
        return SharedInstanceClass.sharedInstance.caseData?.caseFaultServicesArray?[parentIndex].caseFaultServiceSubListArray?[childIndex].serviceDiscString
    }
    
    func tableView(_ tableView: UITableView!, didSelectParentCellAt parentIndex: Int) {
       print("\(String(describing: SharedInstanceClass.sharedInstance.caseData?.caseFaultServicesArray?[parentIndex].caseFaultServiceSubListArray))")
    }
    func shouldDisplaySelectedStateForCell(atChildIndex childIndex: Int, withinParentCellIndex parentIndex: Int) -> Bool {
        return true
    }
        func iconForParentCell(at parentIndex: Int) -> UIImage! {
            return UIImage(named: "arrow-icon")
        }
    func shouldSupportMultipleSelectableChildren(atParentIndex parentIndex: Int) -> Bool {
        return false
    }

    
    //MARK: Textfiled Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
