//
//  LoginViewController.swift
//  DispatcherApp
//
//  Created by varsha on 5/21/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD
import SlideMenuControllerSwift

class LoginViewController: UIViewController {

    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBAction func btnLoginClick(_ sender: UIButton) {

        MBProgressHUD.showAdded(to: self.view, animated: true)
        let userName = userNameTF.text
        let passWord = passwordTF.text
       
        let loginParams: [String: AnyObject] = ["loginId" : userName as AnyObject, "password" : passWord as AnyObject, "deviceTypeId" : "4" as AnyObject, "deviceToken" : UserDefault.deviceToken! as AnyObject]
       
        WebserviceHelper() .callLoginApi(parameters: loginParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)

            let responceMsg = UserLogin(JSONString : response)
            
            if responceMsg?.isSuccess == true{
                UserDefault.isLoginOrNot = true
                  UserDefault.userID = (responceMsg?.userId)!
                    UserDefault.clientID = responceMsg!.clientId!
                    UserDefault.userName = responceMsg!.userName!
                    UserDefault.accessToken = responceMsg?.accessToken!
                self.loginSuccessNavigationToHomeView()
                print("success")
              
                          }
            else
            {
                let errorMsg = responceMsg?.serviceMessage
                CommonMethod().alertMessage("Status", message:errorMsg! as NSString , currentVC: self)
            print("error")
            
            }
            
        }
 
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func loginSuccessNavigationToHomeView () {
        
       
        let storyboard = UIStoryboard(name: storyBoard_C, bundle: nil)
        
        let mainViewController: HomeViewController? = storyboard.instantiateViewController(withIdentifier: homeViewIdentifier_C) as? HomeViewController
        
        let sideMenuController: SideMenuViewController? = storyboard.instantiateViewController(withIdentifier: sideMenuIdentifier_C) as? SideMenuViewController
        
        if let mainViewController: HomeViewController = mainViewController, let sideMenuController = sideMenuController
        {
            let nav = UINavigationController(rootViewController: mainViewController)
            
            let APPDELEGATE :AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            let slideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: sideMenuController)
            APPDELEGATE.window?.rootViewController = slideMenuController
            APPDELEGATE.window?.makeKeyAndVisible()

        }
        
    }

    //MARK: Textfield Delegate Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Dismiss KeyBoard
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

}
