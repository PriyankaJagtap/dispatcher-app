//
//  AllJobsDispatcherTableCell.swift
//  DispatcherApp
//
//  Created by varsha on 7/6/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class AllJobsDispatcherTableCell: UITableViewCell {

    @IBOutlet weak var lblServiceStatus: UILabel!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblCreatedBy: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var lblCreatedAt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
