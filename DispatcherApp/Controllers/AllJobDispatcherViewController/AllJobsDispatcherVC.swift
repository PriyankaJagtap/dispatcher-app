//
//  AllJobsDispatcherVC.swift
//  DispatcherApp
//
//  Created by varsha on 7/6/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import MBProgressHUD

class AllJobsDispatcherVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var jobListArray : [JobsListObj]?
    var jobStatusArray : [JobStatusObj]?
    @IBOutlet weak var tableViewAllJobsDispatcher: UITableView!
    
    @IBAction func sideMenuBtnClicked(_ sender: UIButton) {
        slideMenuController()?.toggleLeft()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jobListArray = [JobsListObj]()
        jobStatusArray = [JobStatusObj]()

        self.registerNIBFile()
        self.callJobsStatusDispatcherWS()
        self.callGetAllJobsDispatcherWS()
        self.navigationController?.navigationBar.isHidden = true

        // Do any additional setup after loading the view.
    }
    func registerNIBFile()
    {
        tableViewAllJobsDispatcher.register(UINib(nibName: "AllJobsDispatcherTableCell", bundle: nil), forCellReuseIdentifier: "allDispatcherJobList")
    
    }
     func callJobsStatusDispatcherWS() {
        
        let jobStatus: [String: AnyObject] = [:]

        WebserviceHelper() .callJobsStatusDispatcherAPI(parameters:jobStatus as! NSMutableDictionary) { (response) in
            
            let responce = AllJobsDispatcher(JSONString : response)
            
              if (responce?.driverStatuses) != nil{
                if let data = responce?.driverStatuses{
                    
                    self.jobStatusArray = data
                    print("Status \(self.jobStatusArray)")
                }
            }
        }
    }
    
    
    func callGetAllJobsDispatcherWS() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let allJobsListParams: [String: AnyObject] =
            ["userId" : UserDefault.userID as AnyObject]
        
        WebserviceHelper() .callAllJobsDispatcherAPI(parameters: allJobsListParams as! NSMutableDictionary) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let responce = AllJobsDispatcher(JSONString : response)
            if responce?.isSuccess == true
            {
                
                if (responce?.jobsList) != nil{
                    print(responce)
                    
                      if let data = responce?.jobsList{
                        
                       self.jobListArray = data
                    }
                    DispatchQueue.main.async {
                        self.tableViewAllJobsDispatcher.reloadData()
                    }
                }
            }
            else
            {
                CommonMethod().alertMessage("Status", message:"Server error", currentVC: self)
            }
        }
        
    }
    
    //table view method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.jobListArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:AllJobsDispatcherTableCell = tableView.dequeueReusableCell(withIdentifier: "allDispatcherJobList") as! AllJobsDispatcherTableCell!
        
        if let job : JobsListObj = self.jobListArray?[indexPath.row]
        {
           
            if let problemName = job.problemName
            {
               cell.lblServiceName.text = "Service: \(problemName)"
                cell.lblServiceStatus.text = problemName

            }
            cell.lblCreatedAt.text = "Created At: \(job.createdTime!)"
            
            if let drivername = job.driverName
            {
            cell.lblCreatedBy.text = "Created By: \(drivername)"
            }
            
            for jobStatus in (jobStatusArray)!
            {
                if (job.statusId == jobStatus.jobStatusId)
                {
                    cell.lblStatus.text = jobStatus.statusName
                }
            }
            
        }
            return cell
    }

}
