//
//  CustomCalendarView.swift
//  DispatcherApp
//
//  Created by Webwerks on 29/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

protocol CustomCalenderViewDelegate {
    func btnOkClicked(_ sender: UIButton, datestring:String)
    func btnCancelClicked(_ sender: UIButton)
}

class CustomCalendarView: UIView,FSCalendarDelegate,FSCalendarDataSource{
    
     var delegate:CustomCalenderViewDelegate?
    
    var dateString : String?
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    @IBAction func okBtnClicked(_ sender: UIButton) {
      if let dateString = dateString, !dateString.isEmpty
      {
        delegate?.btnOkClicked(sender, datestring: dateString)
    }else
        {
         return
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: UIButton) {
        delegate?.btnCancelClicked(sender)
    }

    //MARK: FSCalender Delegate
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
            
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateString = dateFormatter.string(from: date)
    }
    
    func setDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateString = dateFormatter.string(from: date)
        return dateString!
    }
}
