//
//  AppDelegate.swift
//  TRACDriverApp
//
//  Created by satish on 11/4/16.
//  Copyright © 2016 SatishKumar. All rights reserved.
//

import Foundation

class UserDefault{
    class var deviceToken:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: kDeviceToken){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: kDeviceToken)
            UserDefaults.standard.synchronize()
        }
    }
    class var accessToken:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: kAccessToken){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: kAccessToken)
            UserDefaults.standard.synchronize()
        }
    }
    class var isLoginOrNot:Bool
        
        {
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: isLoginOrNot_C)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: isLoginOrNot_C)
        }
    }
    
    class var userID:Int
    {
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: userID_C)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: userID_C) as! Int
        }
    }
    class var clientID:Int
    {
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: clientID_C)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: clientID_C) as! Int
        }
    }
    class var userName:String
    {
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: userName_C)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.object(forKey: userName_C) as! String
        }
    }
 /*
    class var isDefaultNoti:String?{
        
        get{
            if let did =
                UserDefaults.standard.object(forKey: isDefaultNotifi_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: isDefaultNotifi_C)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    
    
    
    class var CurrentDeviceNetwork:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: deviceNetwork_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: deviceNetwork_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var arrOfUpdatedLocations:NSMutableArray?{
        get{
            if let did = UserDefaults.standard.object(forKey: arrOfUpdatedLocations_C){
                return did as? NSMutableArray
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: arrOfUpdatedLocations_C)
            UserDefaults.standard.synchronize()
        }
    }
    

    
    class var savedOptions:NSData?{
        get{
            if let did = UserDefaults.standard.object(forKey: savedOptions_C){
                return did as? NSData
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: savedOptions_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    
    //MARK: - - check for firebase configaration
    
    class var isFireBaseConfig:String?{
        
        get{
            if let did =
                UserDefaults.standard.object(forKey: isFireBaseConfig_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: isFireBaseConfig_C)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    
    
    class var userinfo:[String:AnyObject]?{
        get{
            if let did = UserDefaults.standard.object(forKey: dictUserInfo_C){
                return did as? [String:AnyObject]
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: dictUserInfo_C)
           UserDefaults.standard.synchronize()
        }
    }
   
    
       class var isSuccess:Bool
        {
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: isSuccess_C)
            UserDefaults.standard.synchronize()
        }
        get{
           return UserDefaults.standard.bool(forKey: isSuccess_C)
        }
    }
    
   
    
    //MARK: - User Login Details
    
    class var ISOCode:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: iSOCode_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: iSOCode_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var accessToken:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: accessToken_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: accessToken_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var clientId:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: clientId_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: clientId_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var countryDescription:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: countryDescription_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: countryDescription_C)
           UserDefaults.standard.synchronize()
        }
    }
    class var countryIsdCode:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: countryIsdCode_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: countryIsdCode_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var countryLatitude:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: countryLatitude_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: countryLatitude_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var countryLongitude:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: countryLongitude_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: countryLongitude_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var currencyDescription:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: currencyDescription_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: currencyDescription_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var currency_id:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: currency_id_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: currency_id_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var displayName:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: displayName_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: displayName_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var email:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: kEmail){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: kEmail)
            UserDefaults.standard.synchronize()
        }
    }
    class var expiresIn:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: expiresIn_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: expiresIn_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var firstName:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: kFirstName){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: kFirstName)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var gpsPollingInterval:Double?{
        get{
            if let did = UserDefaults.standard.object(forKey: gpsPollingInterval_C){
                return did as? Double
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: gpsPollingInterval_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var hashedPassword:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: hashedPassword_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: hashedPassword_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var isValidCredentials:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: isValidCredentials_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: isValidCredentials_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var lastName:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: KLastName){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: KLastName)
            UserDefaults.standard.synchronize()
        }
    }
    class var phone:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: kPhone){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: kPhone)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var profilePicURL:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: profilePicURL_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: profilePicURL_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var refreshToken:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: refreshToken_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: refreshToken_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var roleId:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: roleId_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: roleId_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var serviceCode:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: serviceCode_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: serviceCode_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var userId:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: ReqUserId){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: ReqUserId)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var zone_id:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: zone_id_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: zone_id_C)
            UserDefaults.standard.synchronize()
        }
    }
    class var zone_name:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: zone_name_C){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: zone_name_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var pass_word:String?{
        get{
            if let did = UserDefaults.standard.object(forKey: kPassword){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: kPassword)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var autoStatusCheckedIn:Double?{
        
        get{
            if let did =
                UserDefaults.standard.object(forKey: autoStatusCheckedIn_C){
                return did as? Double
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: autoStatusCheckedIn_C)
            UserDefaults.standard.synchronize()
        }
        
    }
    class var autoStatusEnRoute:Double?{
        
        get{
            if let did =
                UserDefaults.standard.object(forKey: autoStatusEnRoute_C){
                return did as? Double
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: autoStatusEnRoute_C)
            UserDefaults.standard.synchronize()
        }
        
    }

    class var autoStatusProceeding:Double?{
        
        get{
            if let did =
                UserDefaults.standard.object(forKey: autoStatusProceeding_C){
                return did as? Double
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: autoStatusProceeding_C)
            UserDefaults.standard.synchronize()
        }
        
    }

    class var assistanceDetails:NSData?{
        get{
            if let did = UserDefaults.standard.object(forKey: assistanceDetails_C){
                return did as? NSData
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: assistanceDetails_C)
            UserDefaults.standard.synchronize()
        }
    }
    
 

    class var commentsList:NSData?{
        get{
            if let did = UserDefaults.standard.object(forKey: commentsList_C){
                return did as? NSData
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: commentsList_C)
            UserDefaults.standard.synchronize()
        }
    }

    
    class var DriverCaseDetails: NSData? {
        
        get{
            if let did = UserDefaults.standard.object(forKey: driverCaseDetails_C){
                return did as? NSData
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: driverCaseDetails_C)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    
    class var Notification:NSData?{
        get{
            if let did = UserDefaults.standard.object(forKey: notification_C){
                return did as? NSData
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: notification_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    class var arrofSavedNotes:NSData?{
        get{
            if let did = UserDefaults.standard.object(forKey: arrofSavedNotes_C){
                return did as? NSData
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: arrofSavedNotes_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    
  
    class var savedSettings:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: savedSettings_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: savedSettings_C)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    class var commentsCount:Int?{
        get{
            if let did = UserDefaults.standard.object(forKey: commentsCount_C){
                return did as? Int
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: commentsCount_C)
            UserDefaults.standard
                
                .synchronize()
        }
    }
   */
}
 
