//
//  CaseDetails.swift
//  DispatcherApp
//
//  Created by varsha on 5/30/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class CaseDetails: Mappable {
    
    static let sharedInstance = CaseDetails()

    init() {
    }
    
    var isSuccess : Bool?
    var caseDetailJobListArray : [CaseDetailsJobListData]?
    var caseCommentListArray : [CaseCommentListData]?
    var caseDetailsDictionary : CaseDetailsData?
    var caseFaultServicesArray : [CaseFaultSErvicesListData]?
    var serviceMessage : String?

    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        caseDetailJobListArray <- map["subcasesInfo"]
        caseCommentListArray <- map["commentsList"]
        caseDetailsDictionary <- map["customer"]
        caseFaultServicesArray <- map["faultservices"]
        serviceMessage <- map["serviceMessage"] //pri
    }

}


struct CaseDetailsData :Mappable {
    var location : String?
    var street : String?
    var mobileNumber : String?
    var firstName : String?
    var lastName : String?
    var emailId : String?
    var alternateMobileNumber : String?
    var areaCode : String?
    var countryCode : String?

    // Vehicle
    var brandAndModel : String?
    var customerLocationCountryName : String?
    var customerLocationCity : String?
    var plateCode : String?
    var vehicleRegisterNo : String?
    var vin : String?
    var vehicleBrandName : String?
    var vehicleModelName : String?
    var mileage : String?
    var vehicleModelYear : String?
    var color : String?

    // Location
    
    var addressTitle : String?
    var area : String?
//    var street : String?
    var landmark : String?
    var building : String?
    var country : String?
    var city : String?
    var poBox : String?


    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        location  <- map["customerLocationName"]
        street  <- map["street"]
        mobileNumber  <- map["mobileNumber"]
        firstName  <- map["firstName"]
        lastName  <- map["lastName"]
        emailId  <- map["emailId"]
        alternateMobileNumber  <- map["alternateMobileNumber"]
        areaCode  <- map["areaCode"]
        countryCode  <- map["countryCode"]

        // Vehicle
        brandAndModel  <- map["brandAndModel"]
        customerLocationCountryName  <- map["customerLocationCountryName"]
        customerLocationCity  <- map["customerLocationCity"]
        plateCode  <- map["plateCode"]
        vehicleRegisterNo  <- map["vehicleRegisterNo"]
        vin  <- map["vin"]
        vehicleBrandName  <- map["vehicleBrandName"]
        vehicleModelName  <- map["vehicleModelName"]
        mileage  <- map["mileage"]
        vehicleModelYear  <- map["vehicleModelYear"]
        color  <- map["color"]

        // Location
        addressTitle  <- map["locationName"]
        area  <- map["area"]
       // street  <- map["street"]
        landmark  <- map["landmark"]
        building  <- map["building"]
        country  <- map["countryName"]
        city  <- map["city"]
        poBox  <- map["pobox"]

        
    }
}

struct CaseDetailsJobListData :Mappable {
    //      Jobs
    var jobID : Int?
    var jobStatus : String?
    var jobService : String?
    var jobCreatedDate : String?
    var jobDriverName : String?
    var jobFaultDescription : String?

    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        // Jobs
        jobID  <- map["subcaseId"]
        jobStatus  <- map["caseStatus"]
        jobService  <- map["FaultServiceName"]
        jobCreatedDate  <- map["createdAt"]
        jobDriverName  <- map["drivername"]
        jobFaultDescription  <- map["faultDescription"]

    }
}

struct CaseCommentListData :Mappable {
    //      Comments
    var loggerName : String?
    var description : String?
    var comment : String?

    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        // Jobs
        loggerName  <- map["loggerName"]
        description  <- map["logDescription"]
        comment  <- map["comments"]
    }
}

// Fault Services
struct CaseFaultSErvicesListData :Mappable {
    var caseFaultServiceName : String?
    var caseFaultServiceSubListArray : [caseFaultServiceSubListArrayData]?
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        caseFaultServiceName  <- map["caseFaultDesc"]
        caseFaultServiceSubListArray <- map["faultServicesList"]
    }
}

struct caseFaultServiceSubListArrayData: Mappable {
    var serviceDiscString : String?
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        serviceDiscString <- map["caseServiceDesc"]
    }
}
