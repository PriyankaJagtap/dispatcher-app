//
//  TruckList.swift
//  DispatcherApp
//
//  Created by Webwerks on 15/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class ProblemServices: Mappable {
    var isSuccess : Bool?
    var problemServicesArray : [ProblemServicesArray]?
    var countriesArray : [CountriesArray]?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        problemServicesArray <- map["problemServices"]
        countriesArray <- map["countries"]

    }
}

struct ProblemServicesArray : Mappable {
    var caseFaultId : String?
    var caseFaultDesc : String?
    var faultServicesList : [FaultServicesList]?

    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        caseFaultId <- map["caseFaultId"]
        caseFaultDesc <- map["caseFaultDesc"]
        faultServicesList <- map["faultServicesList"]
        
    }
}

struct FaultServicesList: Mappable {
    var caseServiceDesc : String?
    var caseServiceId : String?
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        caseServiceDesc <- map["caseServiceDesc"]
        caseServiceId <- map["caseServiceId"]
    }
}

struct CountriesArray : Mappable {
    var countryCodeName : String?
    var countryCode : String?
    var CountryCodeDetail : String?
    var cityName : String?
    var countryIsdCode : String?
    var countryName : String?
    
   // var caseFaultDesc : String?
   // var faultServicesList : [FaultServicesList]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        countryCodeName <- map["ISOCode"]
        countryCode <- map["countryCode"]
        CountryCodeDetail <- map["country_description"]
        cityName <- map["CityName"]
        countryIsdCode <- map["CountryIsdCode"]
        countryName <- map["Name"]

        
        
    }
}
