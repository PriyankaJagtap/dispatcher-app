//
//  SharedInstanceClass.swift
//  DispatcherApp
//
//  Created by varsha on 5/30/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit

class SharedInstanceClass: NSObject {
    static let sharedInstance = SharedInstanceClass()
    
    var selectedCaseID : Int?

    var caseData : CaseDetails?
    
    var casedetail: CaseDetails {
        get{return self.caseData!}
        set{self.caseData = newValue}
    }

    
}
