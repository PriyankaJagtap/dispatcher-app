//
//  ServicesAndBrand.swift
//  DispatcherApp
//
//  Created by Webwerks on 21/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class ServicesAndBrand: Mappable {
    
    var isSuccess : Bool?
    var services : [ServicesArray]?
    var truckBrandList : [TruckBrandListArray]?

    required init?(map: Map) {
        
    }
    public func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        services <- map["services"]
        truckBrandList <- map["truckBrandList"]
        }
}

struct ServicesArray :Mappable {
    var id : Int = 0
    var serviceId : Int = 0
    var description : String = ""
    var checked : Bool = false

    init?(map:Map) {
        
    }
    mutating func mapping(map: Map) {
        id <- map["id"]
        serviceId <- map["serviceId"]
        description <- map["description"]
        checked <- map["checked"]
   }
}

struct TruckBrandListArray :Mappable {
    var brandModels : [BrandModelsArray]?
    var truckBrandName : String?
    var truckBrandID : Int?

    init?(map:Map) {
        
    }
    mutating func mapping(map: Map) {
        brandModels <- map["brandModels"]
        truckBrandName <- map["truckBrandName"]
        truckBrandID <- map["truckBrandID"]

}
}

struct BrandModelsArray :Mappable {
    var truckModelName : String?
    var truckModelID : Int?

    
    init?(map:Map) {
        
    }
    
    mutating func mapping(map: Map) {
        truckModelName <- map["truckModelName"]
        truckModelID <- map["truckModelID"]
        
    }
}
