//
//  VehicleBrandAndModel.swift
//  DispatcherApp
//
//  Created by varsha on 6/21/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class VehicleBrandAndModel: Mappable {
    
    static let sharedInstance = VehicleBrandAndModel()
    init() {
    }
    
    var isSuccess : Bool?
    var vehicleBrandsArray : [VehicleBrandArrayData]?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        vehicleBrandsArray <- map["vehicleBrandsList"]
    }
}

struct VehicleBrandArrayData : Mappable {
    var vehicleBrandModelArray : [VehicleBrandModelArrayData]?
    var vehicleBrandName : String?
    var vehicleBrandId : Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
          vehicleBrandModelArray <- map["brandModels"]
          vehicleBrandName <- map["vehicleBrandName"]
        vehicleBrandId <- map["vehicleBrandId"]
    }
}





struct VehicleBrandModelArrayData : Mappable {
    var vehicleModelName : String?
    var vehicleModelId : Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        vehicleModelName <- map["vehicleModelName"]
        vehicleModelId <- map["vehicleModelId"]
    }
}
