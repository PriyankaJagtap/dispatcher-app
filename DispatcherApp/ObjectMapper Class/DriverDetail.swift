//
//  DriverDetail.swift
//  DispatcherApp
//
//  Created by varsha on 7/18/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class DriverDetail: Mappable {
    var isSuccess : Bool?
    var driverInfoDict : DriverInfoData?
    var driverExpList : [DriverExpList]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        driverInfoDict <- map["driverInfo"]
        driverExpList <- map["driverExpList"]
    }
}

struct DriverInfoData : Mappable {
    var employeeId : String?
    var phone : String?
    var passportNo : String?
    var countryISO : String?
    var street : String?
    var reportingTo : Int?
    var presentdesignation : String?
    var trouserSize : String?
   
    var eContactName : String?
     var eArea : String?
    var eBuilding : String?
    var ePhone : String?
    var emergencycountryIsoCodeCountryName : String?
    var eCountryISO : String?
    var eCountry : String?
    var eEmail : String?
    var eCity : String?
    var eStreet : String?
    var ePostBox : String?
    var ecountryAreaCode : String?
    var eLandmark : String?

    
    var visaNo : String?
     var city : String?
     var shirtSize : String?
     var noOfTrousersReceived : String?
     var userName : String?
     var countryAreaCode : String?
     var preferedWorkTimings : Int?
    var licenseNoExpiredAt : String?
    var countryIsoCodeCountryName : String?
    var visaIssuedAt : String?
    var shoeSize : String?
    var driverFirstName : String?
    var presentDateOfJoining : String?
    var passportExpiredAt : String?
    var country : String?
    var landmark : String?
    var email : String?
    var healthInsuranceCardExpiryAt : String?
    var preferedShiftTimings : Int?
    var preferedTruck : String?
    var governmentIdNumber : String?
    var passportIssuedAt : String?
    var reportingManager : String?
    var noOfShoeReceived : String?
    var preferedDayOff : Int?
    var preferedTruckId : Int?
    var area : String?
    var licenseNo : String?
    var noOfShirtsReceived : String?
    var building : String?
    var visaExpiredAt : String?
   
    var healthInsuranceCardNo : String?
    var postBox : String?
    var driverLastName : String?
    var reportingUserId : String?
    var nationality : String?
    var driverID : Int?

    
    
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        employeeId <- map["employeeId"]
        phone <- map["phone"]
        passportNo <- map["passportNo"]
        eArea <- map["eArea"]
        countryISO <- map["countryISO"]
        street <- map["street"]
        reportingTo <- map["reportingTo"]
        presentdesignation <- map["presentdesignation"]
        trouserSize <- map["trouserSize"]
        visaNo <- map["visaNo"]
        eBuilding <- map["eBuilding"]
        city <- map["city"]
        shirtSize <- map["shirtSize"]
        eContactName <- map["eContactName"]
        noOfTrousersReceived <- map["noOfTrousersReceived"]
        userName <- map["userName"]
        countryAreaCode <- map["countryAreaCode"]
        preferedWorkTimings <- map["preferedWorkTimings"]
        licenseNoExpiredAt <- map["licenseNoExpiredAt"]
        ePhone <- map["ePhone"]
        countryIsoCodeCountryName <- map["countryIsoCodeCountryName"]
        visaIssuedAt <- map["visaIssuedAt"]
        shoeSize <- map["shoeSize"]
        driverFirstName <- map["driverFirstName"]
        emergencycountryIsoCodeCountryName <- map["emergencycountryIsoCodeCountryName"]
        presentDateOfJoining <- map["presentDateOfJoining"]
        passportExpiredAt <- map["passportExpiredAt"]
        eCountryISO <- map["eCountryISO"]
        country <- map["country"]
        landmark <- map["landmark"]
        email <- map["email"]
        healthInsuranceCardExpiryAt <- map["trouserSize"]
        preferedShiftTimings <- map["trouserSize"]
        eCountry <- map["eCountry"]
        preferedTruck <- map["preferedTruck"]
        governmentIdNumber <- map["governmentIdNumber"]
        passportIssuedAt <- map["passportIssuedAt"]
        reportingManager <- map["reportingManager"]
        noOfShoeReceived <- map["noOfShoeReceived"]
        eEmail <- map["eEmail"]
        preferedDayOff <- map["preferedDayOff"]
        eCity <- map["eCity"]
        preferedTruckId <- map["preferedTruckId"]
        area <- map["area"]
        licenseNo <- map["licenseNo"]
        noOfShirtsReceived <- map["noOfShirtsReceived"]
        building <- map["building"]
        visaExpiredAt <- map["visaExpiredAt"]
        eStreet <- map["eStreet"]
        ePostBox <- map["ePostBox"]
        healthInsuranceCardNo <- map["healthInsuranceCardNo"]
        postBox <- map["postBox"]

        driverLastName <- map["driverLastName"]
        reportingUserId <- map["reportingUserId"]
        nationality <- map["nationality"]
        ecountryAreaCode <- map["ecountryAreaCode"]
        driverID <- map["driverID"]
        eLandmark <- map["eLandmark"]

          }
}

struct DriverExpList : Mappable {
    var dateOfJoining : String?
    var dateOfResign : String?
    var totalExperience : String?
    var designation : String?
    var experienceID : String?
    var companyName : String?
    var relatedExp : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        dateOfJoining <- map["doj"]
        dateOfResign <- map["dor"]
        totalExperience <- map["totalExperience"]
        designation <- map["designation"]
        companyName <- map["companyName"]
        relatedExp <- map["relatedExpirence"]
        experienceID <- map["exprerienceID"]
      
        
    }
}



