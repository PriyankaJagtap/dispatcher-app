//
//  TruckList.swift
//  DispatcherApp
//
//  Created by Webwerks on 15/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class TruckList: Mappable {
    var isSuccess : Bool?
    var truckList : [truckListObj]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        truckList <- map["truckList"]
    }
}

struct truckListObj : Mappable {
    var truckCode : String?
    var truckPolicyIssuedDate : String?
    var truckReRegDueDate  : String?
    var truckId : Int?
    var lastcaseAttendedTime : String?
    var truckNickName : String?
    var truckPolicyExpDate : String?
    var dateOfAcquiring : String?
    var plateCode : String?
    var truckBrandName : String?
    var dateOfPurchase : String?
    var registrationDate : String?
    var vin : String?
    var modelYear : String?
    var plateNumber : String?
    var truckModelName : String?
    var truckPolicyNumber : String?
    var insCompanyName : String?
    var truckBrandID : Int?
    var allservicesList : [allServicesData]?
    var truckModelID : Int?
    var truckColor : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        truckCode <- map["truckCode"]
        truckPolicyIssuedDate <- map["truckPolicyIssuedDate"]
        truckReRegDueDate <- map["truckReRegistrationDueDate"]
        truckId <- map["truckId"]
        lastcaseAttendedTime <- map["lastcaseAttendedTime"]
        truckNickName <- map["truckNickName"]
        truckPolicyExpDate <- map["truckPolicyExpiryDate"]
        dateOfAcquiring <- map["dateOfAcquiring"]
        plateCode <- map["plateCode"]
        truckBrandName <- map["truckBrandName"]
        dateOfPurchase <- map["dateOfPurchase"]
        registrationDate <- map["registrationDate"]
        vin <- map["vin"]
        modelYear <- map["modelYear"]
        plateNumber <- map["plateNumber"]
        truckModelName <- map["truckModelName"]
        truckPolicyNumber <- map["truckPolicyNumber"]
         insCompanyName <- map["insCompanyName"]
       truckBrandID <- map["truckBrandID"]
        allservicesList <- map["allservicesList"]
        truckModelID <- map["truckModelID"]
        truckColor <- map["truckColor"]
}
}

struct allServicesData:Mappable {
    var id : Int = 0
    var serviceId : Int = 0
    var description : String = ""
    var checked : Bool = false
    
    init?(map:Map) {
        
    }
    mutating func mapping(map: Map) {
        id <- map["id"]
        serviceId <- map["serviceId"]
        description <- map["description"]
        checked <- map["checked"]
    }
}



