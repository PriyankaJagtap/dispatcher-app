//
//  TruckAndShiftManager.swift
//  DispatcherApp
//
//  Created by Webwerks on 12/07/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class TruckAndShiftManager: Mappable {

      var isSuccess : Bool?
    var shiftList : [ShiftListObj]?
    var managers : [ManagersObj]?
    var truckList : [TruckListAndCodeObj]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
       isSuccess <-  map["isSuccess"]
        shiftList <- map["shiftList"]
       managers <- map["managers"]
        truckList <- map["truckList"]
    }
}

struct ShiftListObj : Mappable {
    var shiftName : String?
    var shiftDayId : Int?
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        shiftName <-  map["shiftName"]
        shiftDayId <-  map["shiftDayId"]


    }
}

struct ManagersObj : Mappable {
    var userID : Int?
    var displayName : String?
    var firstName : String?
    var lastName : String?

    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        userID <-  map["userID"]
        displayName <-  map["displayName"]
        firstName <-  map["firstName"]
        lastName <-  map["lastName"]

    }
}

struct TruckListAndCodeObj : Mappable {
    var truckId : Int?
    var truckCode : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        truckId <-  map["truckId"]
        truckCode <-  map["truckCode"]
 
    }
}



