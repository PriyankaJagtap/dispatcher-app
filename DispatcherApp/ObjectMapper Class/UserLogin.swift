//
//  UserLogin.swift
//  DispatcherApp
//
//  Created by varsha on 5/22/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import Foundation
import ObjectMapper


class UserLogin: Mappable {
   
    var serviceMessage : String?
    var isSuccess : Bool?
    var clientId : Int?
    var userId : Int?
    var accessToken : String?
    var userName : String?
    
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        serviceMessage <- map["serviceMessage"]
        isSuccess <- map["isSuccess"]
        clientId  <- map["clientId"]
        userId  <- map["userId"]
        accessToken  <- map["accessToken"]
        userName  <- map["firstName"]

    }
}


