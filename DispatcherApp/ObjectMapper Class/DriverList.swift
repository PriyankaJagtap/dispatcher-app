//
//  DriverList.swift
//  DispatcherApp
//
//  Created by webwerks on 14/06/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import Foundation
import ObjectMapper


class DriverList : Mappable {
    var isSuccess : Bool?
    var driverListArray : [DriverListData]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        driverListArray <- map["drivers"]
    }
}


struct DriverListData : Mappable {
    var Id : String?
    var driverId : Int?
    var driverName : String?
    var driverExpiryDetail : String?
    var driverPhoneNumber : String?
    var driverShift : String?
    var driverPicUrl : String?
    var driverGPSLocationLat : String?
    var driverGPSLocationLong : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        Id <- map["Id"]
        driverId <- map["driverID"]
        driverName <- map["driverName"]
        driverExpiryDetail <- map["expiryDetail"]
        driverPhoneNumber <- map["phone"]
        driverShift <- map["shiftName"]
        driverPicUrl <- map["driverPicUrl"]
        driverGPSLocationLat <- map["GPSLocationLat"]
        driverGPSLocationLong <- map["GPSLocationLong"]
        
    }
}
