//
//  AllJobsDispatcher.swift
//  DispatcherApp
//
//  Created by Webwerks on 07/07/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class AllJobsDispatcher: Mappable {

    var isSuccess : Bool?
    var jobsList : [JobsListObj]?
    var driverStatuses : [JobStatusObj]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        jobsList <- map["jobsList"]
        driverStatuses <- map["driverStatuses"]
    }
}

struct JobsListObj :Mappable{
    
    var jobId : Int?
    var truckNickname : String?
    var problemName : String?
    var createdTime : String?
    var driverName : String?
    var statusId : Int?
    
     init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        jobId <- map["jobId"]
        truckNickname <- map["truckNickname"]
        problemName <- map["problemName"]
        createdTime <- map["createdTime"]
        driverName <- map["driverName"]
        statusId <- map["statusId"]

    }
}

struct JobStatusObj : Mappable {
   
    var jobStatusId : Int?
    var statusName : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        jobStatusId <- map["statusId"]
        statusName <- map["statusName"]
        }

}
