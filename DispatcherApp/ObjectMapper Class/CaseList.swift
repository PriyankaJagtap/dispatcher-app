//
//  CaseList.swift
//  DispatcherApp
//
//  Created by varsha on 5/23/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import ObjectMapper

class CaseList: Mappable {

    var isSuccess : Bool?
    var caseListArray : [CaseListData]?

    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        isSuccess <- map["isSuccess"]
        caseListArray <- map["caseslist"]
    }

}

struct CaseListData :Mappable {
    var customerName : String?
    var caseID : Int?
    var location : String?
    var vehicle : String?
    var status : String?
    var mobileNumber : String?
    
    init?(map: Map) {
    
    }
    
    mutating func mapping(map: Map) {
        customerName  <- map["callername"]
        caseID  <- map["caseId"]
        location  <- map["countryName"]
        vehicle  <- map["vehicleRegisterNo"]
        status  <- map["caseStatus"]
        mobileNumber  <- map["phone"]
    }
}
