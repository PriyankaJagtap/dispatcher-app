//
//  AppDelegate.swift
//  DispatcherApp
//
//  Created by varsha on 5/21/17.
//  Copyright © 2017 varsha. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import Firebase
import FirebaseMessaging
import GoogleMaps
import SlideMenuControllerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, FIRMessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(googleMapsKey_C)
        FIRApp .configure()

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
         NotificationCenter.default.addObserver(self, selector:#selector(tokenRefreshNotification(notification:)), name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        


        self.setUpRootView()
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage){
    }
    
    func tokenRefreshNotification(notification : NSNotification){
        let refreshedtoken  = FIRInstanceID.instanceID().token()
        if refreshedtoken != nil{
            UserDefault.deviceToken = refreshedtoken!
//            self.isDeviceTokeAvai = true
//            if UserDefault.isLoginOrNot{
//                updateDeviceTokenToServer(deviceToken: refreshedtoken!)
//            }
        }
        else{
            UserDefault.deviceToken  = ""
        }
        connecettoFcm()
    }
    func connecettoFcm(){
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil{
            }
            else{
            }
        }
    }
    func setUpRootView() -> Void {
        let storyboard = UIStoryboard(name: storyBoard_C, bundle: nil)
        let navigationController : UINavigationController!
        if UserDefault.isLoginOrNot {

            let mainViewController: HomeViewController? = storyboard.instantiateViewController(withIdentifier: homeViewIdentifier_C) as? HomeViewController
            
            let sideMenuController: SideMenuViewController? = storyboard.instantiateViewController(withIdentifier: sideMenuIdentifier_C) as? SideMenuViewController
            
            if let mainViewController: HomeViewController = mainViewController, let sideMenuController = sideMenuController
            {
                
                let nav = UINavigationController(rootViewController: mainViewController)
                
                let slideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: sideMenuController)
                self.window?.rootViewController = slideMenuController
                self.window?.makeKeyAndVisible()
            }

            
            //let homeVc = storyboard.instantiateViewController(withIdentifier: homeViewIdentifier_C) as! HomeViewController
           // homeVc.strViewFrom = appDelegateView_C
           // navigationController = UINavigationController.init(rootViewController: homeVc)
           // navigationController.isNavigationBarHidden = true
       }
        else{
           let loginVC = storyboard.instantiateViewController(withIdentifier: loginViewIdentifier_C) as! LoginViewController
            navigationController = UINavigationController.init(rootViewController: loginVC)
            navigationController.isNavigationBarHidden = true
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

